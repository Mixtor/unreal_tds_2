
#include "ProjectileDefaults.h"
// #include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "HealthComponent.h"
#include "GameFramework/Character.h"

// Sets default values
AProjectileDefaults::AProjectileDefaults()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));

	Collision->SetSphereRadius(16.f);

	Collision->bReturnMaterialOnMove = true;// hit event return physMaterial

	Collision->SetCanEverAffectNavigation(false);// collision not affect navigation (P Keyboard on editor)

	RootComponent = Collision;

	Bullet = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	Bullet->SetupAttachment(RootComponent);
	Bullet->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	//BulletSound = CreateDefaultSubobject<UAudioComponent>(TEXT("Bullet Audio"));
	//BulletSound->SetupAttachment(RootComponent);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	ProjectileMovement->UpdatedComponent = RootComponent;
	ProjectileMovement->InitialSpeed = 1.f;
	ProjectileMovement->MaxSpeed = 0.f;

	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void AProjectileDefaults::BeginPlay()
{
	Super::BeginPlay();
	
	Collision->OnComponentHit.AddDynamic(this, &AProjectileDefaults::ProjectileHit);
	Collision->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefaults::ProjectileBeginOverlap);
	Collision->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefaults::ProjectileEndOverlap);
}

// Called every frame
void AProjectileDefaults::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AProjectileDefaults::InitProjectile(FProjectileInfo InitParam, bool Debug)
{
	ProjectileMovement->InitialSpeed = InitParam.ProjectileInitSpeed;
	ProjectileMovement->MaxSpeed = InitParam.ProjectileInitSpeed;
	ProjectileMovement->ProjectileGravityScale = InitParam.Bomb;
	this->SetLifeSpan(InitParam.ProjectileLifeTime);

	// if (InitParam.ProjectileStaticMesh)
	// {
	// 	Bullet->SetStaticMesh(InitParam.ProjectileStaticMesh);
	// 	Bullet->SetRelativeTransform(InitParam.ProjectileStaticMeshOffset);
	// }
	// else Bullet->DestroyComponent();

	if (InitParam.ProjectileTrailFx)
	{
		BulletFX->SetTemplate(InitParam.ProjectileTrailFx);
		BulletFX->SetRelativeTransform(InitParam.ProjectileTrailFxOffset);
	}
	else BulletFX->DestroyComponent();
	
	ProjectileSettings = InitParam;

	ProjectileDebug = Debug;
}

void AProjectileDefaults::ProjectileHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if(OtherActor && Hit.PhysMaterial.IsValid())
	{
		const EPhysicalSurface SurfaceType = UGameplayStatics::GetSurfaceType(Hit);

		if(ProjectileSettings.HitDecals.Contains(SurfaceType))
		{
			UMaterialInterface* MyMaterial = ProjectileSettings.HitDecals[SurfaceType];

			if(MyMaterial && OtherComp)
			{
				UGameplayStatics::SpawnDecalAttached(MyMaterial, FVector(20.0f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(),
											EAttachLocation::KeepWorldPosition, 10.0f);
			}
		}

		if(ProjectileSettings.HitFXs.Contains(SurfaceType))
		{
			UParticleSystem* ParticleSystem = ProjectileSettings.HitFXs[SurfaceType];
			if(ParticleSystem)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleSystem, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
			}
		}
		
		if(ProjectileSettings.HitSound)
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSettings.HitSound, Hit.ImpactPoint);
		}

		UTypes::AddEffectBySurfaceType(Hit, ProjectileSettings.Effect, SurfaceType);
	}

	if(ProjectileSettings.Bomb)
	{
		if(ProjectileSettings.ExplosionFX)
		{
			UParticleSystem* ParticleSystem = ProjectileSettings.ExplosionFX;
			
			if(ParticleSystem)
			{
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleSystem, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
			}
		}

		AddExplosionDamage();
	}
	else if(IsValid(OtherActor))
	{
		UActorComponent* ActorComponent = OtherActor->GetComponentByClass(UHealthComponent::StaticClass());
		if(UHealthComponent* HealthComponent = Cast<UHealthComponent>(ActorComponent))
		{
			HealthComponent->ChangeHealthValue(-ProjectileSettings.ProjectileDamage);
		}
	}
	
	if(ProjectileDebug)
	{
		DrawDebugSphere(GetWorld(), Hit.Location, ProjectileSettings.ExplosionInnerRadius, 8, FColor::Red, false, 4.0f);
		DrawDebugSphere(GetWorld(), Hit.Location, ProjectileSettings.ExplosionOuterRadius, 8, FColor::Yellow, false, 4.0f);
	}

	// ProjectileImpact();
}

void AProjectileDefaults::ProjectileBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
												bool bFromSweep, const FHitResult& SweepResult)
{
}

void AProjectileDefaults::ProjectileEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AProjectileDefaults::ProjectileImpact()
{
	this->Destroy();
}

void AProjectileDefaults::AddExplosionDamage()
{
	TArray<AActor*> OverlappingActors;
	TArray<AActor*> DestroyableActors;
	
	USphereComponent* ExpRadius = NewObject<USphereComponent>(this);
	ExpRadius->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	ExpRadius->RegisterComponent();
	ExpRadius->SetSphereRadius(ProjectileSettings.ExplosionOuterRadius);
	
	ExpRadius->GetOverlappingActors(OverlappingActors);
	
	for(auto& Actor : OverlappingActors)
	{
		UActorComponent* ActorComponent = Actor->GetComponentByClass(UHealthComponent::StaticClass());
		if(UHealthComponent* HealthComponent = Cast<UHealthComponent>(ActorComponent))
		{
			if(GetDistanceTo(Actor) <= ProjectileSettings.ExplosionInnerRadius)
			{
				HealthComponent->ChangeHealthValue(-ProjectileSettings.ExplosionMaxDamage);
			}
			else
			{
				if(ProjectileSettings.ExplosionFalloffCoefficient == 0)
				{
					HealthComponent->ChangeHealthValue(-ProjectileSettings.ExplosionMaxDamage);
				}
				else
				{
					FVector2D InputRange (ProjectileSettings.ExplosionInnerRadius, ProjectileSettings.ExplosionOuterRadius);
					FVector2D OutputRange (ProjectileSettings.ExplosionMaxDamage, 1.0f);
				
					float MaximumDamage = FMath::GetMappedRangeValueClamped(InputRange, OutputRange, GetDistanceTo(Actor));
					float ExplosionDamage = FMath::RoundToFloat(MaximumDamage / ProjectileSettings.ExplosionFalloffCoefficient);
				
					HealthComponent->ChangeHealthValue(-ExplosionDamage);
				}
			}
		}
	}
}
