
#include "WeaponDefaults.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "TDSInventoryComponent.h"

// Sets default values
AWeaponDefaults::AWeaponDefaults()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	Weapon_Anim = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	Weapon_Anim->SetGenerateOverlapEvents(false);
	Weapon_Anim->SetCollisionProfileName(TEXT("NoCollision"));
	Weapon_Anim->SetupAttachment(RootComponent);

	Weapon_Static = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	Weapon_Static->SetGenerateOverlapEvents(false);
	Weapon_Static->SetCollisionProfileName(TEXT("NoCollision"));
	Weapon_Static->SetupAttachment(RootComponent);

	Projectile_SP = CreateDefaultSubobject<UArrowComponent>(TEXT("Projectile_SP"));
	Projectile_SP->SetupAttachment(RootComponent);

	Shell_SP = CreateDefaultSubobject<UArrowComponent>(TEXT("Shell_SP"));
	Shell_SP->SetupAttachment(RootComponent);

	Clip_SP = CreateDefaultSubobject<UArrowComponent>(TEXT("Clip_SP"));
	Clip_SP->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefaults::BeginPlay()
{
	Super::BeginPlay();

	RecogniseWeapon();
}

// Called every frame
void AWeaponDefaults::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
}

void AWeaponDefaults::FireTick(float DeltaTime)
{
	if (GetWeaponClip() > 0)
	{
		if (Shooting)
			if (ShotTimer < 0.f)
			{
				if (!WeaponReloading)
					Shot();
			}
			else
				ShotTimer -= DeltaTime;
	}
	else
	{
		if (!WeaponReloading)
		{
			InitReload();
		}
	}
}

void AWeaponDefaults::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefaults::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!Shooting)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	/*if (ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);*/
}

void AWeaponDefaults::RecogniseWeapon()
{
	if (Weapon_Anim && !Weapon_Anim->SkeletalMesh)
	{
		Weapon_Anim->DestroyComponent(true);
	}

	if (Weapon_Static && !Weapon_Static->GetStaticMesh())
	{
		Weapon_Static->DestroyComponent();
	}

	UpdateWeaponState(EMovementState::Run_State);
}

void AWeaponDefaults::SetWeaponFireState(bool bIsFire)
{
	Shooting = bIsFire;
	if (CheckWeaponCanFire())
		Shooting = bIsFire;
	else
		Shooting = false;
		ShotTimer = 0.01f;     // !!!!!

}

bool AWeaponDefaults::CheckWeaponCanFire()
{
	return !BlockFire;
}

FProjectileInfo AWeaponDefaults::GetProjectile()
{
	return WeaponSetting.ProjectileSettings;
}

void AWeaponDefaults::Shot()
{
	if (FireRateControl)
	{
		ShotTimer = NewFireRate;
	}
	else
	{
		ShotTimer = WeaponSetting.RateOfFire;
	}

	ChangeDispersionByShot();

	FVector Projectile_SL = Projectile_SP->GetComponentLocation() + Projectile_SP->GetForwardVector()*7;
	FRotator Projectile_SR = Projectile_SP->GetComponentRotation();
	FVector ShotVector = Projectile_SP->GetForwardVector();
	FName Gun_AP = Projectile_SP->GetFName();

	FProjectileInfo ProjectileInfo = GetProjectile();

	if (ProjectileInfo.Projectile != nullptr)
	{
		FireMoment();
		WeaponInfo.Clip = WeaponInfo.Clip - 1;
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.WeaponShootingSound, Projectile_SL);
		UNiagaraFunctionLibrary::SpawnSystemAttached(WeaponSetting.WeaponShootingEffect, RootComponent, Gun_AP, Projectile_SL + ShotVector * 10, Projectile_SR,
											EAttachLocation::KeepWorldPosition, false);
		int8 ProjectileNumber = GetProjectilesByShot();

		for(int8 i = 0; i < ProjectileNumber; i++)// Shotgun
		{
			FVector EndLocation = GetShotEndLocation();

			FVector Dir = EndLocation - Projectile_SL;

			Dir.Normalize();

			FMatrix MyMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
			Projectile_SR = MyMatrix.Rotator();

			if(ProjectileInfo.Projectile)
			{
				//Projectile Init ballistic Fire
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefaults* Projectile = Cast<AProjectileDefaults>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &Projectile_SL, &Projectile_SR,
																			SpawnParams));
				if(Projectile)
				{
					Projectile->InitProjectile(WeaponSetting.ProjectileSettings, ShowDebug);
				}
			}
			else
			{
				//ToDo Projectile null Init trace fire			
				//GetWorld()->LineTraceSingleByChannel()

				// FHitResult Hit;
				//
				// TArray<AActor*> Actors;				
				// EDrawDebugTrace::Type DebugTrace;
				// DebugTrace = EDrawDebugTrace::None;
				// UKismetSystemLibrary::LineTraceSingle(GetWorld(), Projectile_SL, EndLocation,TraceTypeQuery4,
				// 	false, Actors, DebugTrace, Hit, true, FLinearColor::Red,FLinearColor::Green, 5.0f);
				//
				// if(Hit.GetActor() && Hit.PhysMaterial.IsValid())
				// {
				// 	EPhysicalSurface SurfaceType = UGameplayStatics::GetSurfaceType(Hit);
				// 	
				// 	if(WeaponSetting.ProjectileSettings.HitDecals.Contains(SurfaceType))
				// 	{
				// 		UMaterialInterface* myMaterial = WeaponSetting.ProjectileSettings.HitDecals[SurfaceType];
				//
				// 		if(myMaterial && Hit.GetComponent())
				// 		{
				// 			UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint,
				// 												Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
				// 		}
				// 	}
				// 	if (WeaponSetting.ProjectileSettings.HitFXs.Contains(SurfaceType))
				// 	{
				// 		UParticleSystem* myParticle = WeaponSetting.ProjectileSettings.HitFXs[SurfaceType];
				// 		if(myParticle)
				// 		{
				// 			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint,
				// 												FVector(1.0f)));
				// 		}
				// 	}
				//
				// 	if(WeaponSetting.ProjectileSettings.HitSound)
				// 	{
				// 		UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.ProjectileSettings.HitSound, Hit.ImpactPoint);
				// 		GEngine->AddOnScreenDebugMessage(-1,2.f, FColor::White, "SOUND");
				// 	}
				//
				// 	UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileInfo.Effect, SurfaceType);				
					
					//if (Hit.GetActor()->GetClass()->ImplementsInterface(UTPS_IGameActor::StaticClass()))
					//{
					//	//ITPS_IGameActor::Execute_AviableForEffects(Hit.GetActor());
					//	//ITPS_IGameActor::Execute_AviableForEffectsBP(Hit.GetActor());
					//}
												
				// 	UGameplayStatics::ApplyPointDamage(Hit.GetActor(), WeaponSetting.ProjectileSettings.ProjectileDamage, Hit.TraceStart,Hit, GetInstigatorController(),
				// 								this,nullptr);
				// }
			}
		}
		if (WeaponSetting.ShellEject != nullptr)
		{
			FVector Shell_SL = Shell_SP->GetComponentLocation();
			FRotator Shell_SR = Shell_SP->GetComponentRotation();

			UNiagaraComponent* ShellSpawner = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), WeaponSetting.ShellEject, Shell_SL, Shell_SR);

			ShellSpawner->SetVariableObject("ParticleMesh", WeaponSetting.BulletShell.DropMesh);
		}
	}
	else GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("NO PROJECTILES ASSIGNED"));
}

void AWeaponDefaults::UpdateWeaponState(EMovementState NewMovementState)
{
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:

		CurrentDispersionMax = WeaponSetting.Dispersion.Aim_StateDispersionMax;
		CurrentDispersionMin = WeaponSetting.Dispersion.Aim_StateDispersionMin;
		CurrentDispersionRecoil = WeaponSetting.Dispersion.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.Dispersion.Aim_StateDispersionReduction;
		break;
	case EMovementState::Walk_State:

		CurrentDispersionMax = WeaponSetting.Dispersion.Move_StateDispersionMax;
		CurrentDispersionMin = WeaponSetting.Dispersion.Move_StateDispersionMin;
		CurrentDispersionRecoil = WeaponSetting.Dispersion.Move_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.Dispersion.Move_StateDispersionReduction;
		break;
	case EMovementState::Run_State:

		CurrentDispersionMax = WeaponSetting.Dispersion.Move_StateDispersionMax;
		CurrentDispersionMin = WeaponSetting.Dispersion.Move_StateDispersionMin;
		CurrentDispersionRecoil = WeaponSetting.Dispersion.Move_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.Dispersion.Move_StateDispersionReduction;
		break;
	case EMovementState::Sprint_State:
		BlockFire = true;
		SetWeaponFireState(false);
		break;
	default:
		break;
	}
}

void AWeaponDefaults::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + AddDispersion;
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefaults::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefaults::ApplyDispersionToShot(FVector ShotDirection) const
{
	return FMath::VRandCone(ShotDirection, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponDefaults::GetShotEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);

	FVector tmpV = (Projectile_SP->GetComponentLocation() - ShotEndLocation);
	//UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f. Y = %f. Size = %f"), tmpV.X, tmpV.Y, tmpV.Size());

	if (tmpV.Size() > VecSizeToChangeShotDirLogic)
	{
		EndLocation = Projectile_SP->GetComponentLocation() + ApplyDispersionToShot((Projectile_SP->GetComponentLocation() -
																						ShotEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), Projectile_SP->GetComponentLocation(), -(Projectile_SP->GetComponentLocation() - ShotEndLocation),
				WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald,
				false, .1f, (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = Projectile_SP->GetComponentLocation() + ApplyDispersionToShot(Projectile_SP->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), Projectile_SP->GetComponentLocation(), Projectile_SP->GetForwardVector(), WeaponSetting.DistanceTrace, 
				GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Yellow, false, .1f, (uint8)'\000', 1.0f);
	}
	if (ShowDebug)
	{
		// Direction Weapon Look
		DrawDebugLine(GetWorld(), Projectile_SP->GetComponentLocation(), Projectile_SP->GetComponentLocation() + Projectile_SP->GetForwardVector() * 500.0f,
						FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		// Direction Projectile must fly
		DrawDebugLine(GetWorld(), Projectile_SP->GetComponentLocation(), ShotEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		// Direction Projectile currently fly
		DrawDebugLine(GetWorld(), Projectile_SP->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		DrawDebugSphere(GetWorld(), Projectile_SP->GetComponentLocation() + Projectile_SP->GetForwardVector()*VecSizeToChangeShotDirLogic, 10.f, 8,
						FColor::Red, false, 4.0f);
	}

	return EndLocation;
}

int8 AWeaponDefaults::GetProjectilesByShot() const
{
	return WeaponSetting.ProjectilesByShot;
}

int32 AWeaponDefaults::GetWeaponClip()
{
	return WeaponInfo.Clip;
}

void AWeaponDefaults::FireMoment()
{
	OnWeaponFireStart.Broadcast(WeaponSetting.AnimCharShoot);
}

void AWeaponDefaults::InitReload()
{
	WeaponReloading = true;

	ReloadTimer = WeaponSetting.ReloadTime;
	 
	if (WeaponSetting.ClipEject != nullptr)
	{
		FVector Clip_SL = Clip_SP->GetComponentLocation();
		FRotator Clip_SR = Clip_SP->GetComponentRotation();

		UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), WeaponSetting.ClipEject, Clip_SL, Clip_SR);
	}

	if (WeaponSetting.AnimCharReload || WeaponSetting.WeaponReloadingSound)
		OnWeaponReloadStart.Broadcast(WeaponSetting.AnimCharReload, WeaponSetting.WeaponReloadingSound);
}

void AWeaponDefaults::FinishReload()
{
	WeaponReloading = false;
	WeaponInfo.Clip = WeaponSetting.MaxClip;

	OnWeaponReloadEnd.Broadcast(WeaponReloading, WeaponInfo.Clip);
}

void AWeaponDefaults::CancelReload()
{
	WeaponReloading = false;

	if (Weapon_Anim && Weapon_Anim->GetAnimInstance())
		Weapon_Anim->GetAnimInstance()->StopAllMontages(0.15f);
	
	OnWeaponReloadEnd.Broadcast(false, 0);
	DropClipFlag = false;
}

bool AWeaponDefaults::CheckCanWeaponReload()
{
	bool Result = true;
	if (GetOwner())
	{
		UTDSInventoryComponent* MyInv = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if (MyInv)
		{
			int8 AvailableAmmoForWeapon;
			if (!MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvailableAmmoForWeapon))
			{
				Result = false;
			}
		}
	}

	return Result;
}

int8 AWeaponDefaults::GetAvailableAmmoForReload()
{
	int8 AvailableAmmoForWeapon = WeaponSetting.MaxClip;
	if (GetOwner())
	{
		UTDSInventoryComponent* MyInv = Cast<UTDSInventoryComponent>(GetOwner()->GetComponentByClass(UTDSInventoryComponent::StaticClass()));
		if (MyInv)
		{
			if (MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvailableAmmoForWeapon))
			{
				AvailableAmmoForWeapon = AvailableAmmoForWeapon;
			}
		}
	}
	return AvailableAmmoForWeapon;
}