// Fill out your copyright notice in the Description page of Project Settings.

#include "Item_Energy.h"
#include "TDSCharacter.h"

void UItem_Energy::Use(ATDSCharacter* Character)
{
	if (Character)
	{
		Character->Health += HealthToHeal;
	}

	if (OwningInventory)
	{
		OwningInventory->RemoveItem(this);
	}
	
}


