// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"

// #include <functional>

#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "WeaponDefaults.h"
#include "TDSGameInstance.h"
#include "Item.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Inventory/InventoryWidget.h"
#include "Misc/OutputDeviceNull.h"


ATDSCharacter::ATDSCharacter()
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f); // Set Size for Player Capsule
	//-------------------------------------------------------------------------------------- Don't rotate Character to Camera Direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	//-------------------------------------------------------------------------------------- Configure Character Movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	//-------------------------------------------------------------------------------------- Create a CameraBoom
	CamBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("SAC"));
	CamBoom->SetupAttachment(RootComponent);
	CamBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CamBoom->TargetArmLength = 800.f;
	CamBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CamBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
	//-------------------------------------------------------------------------------------- Create a Camera
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CC"));
	TopDownCameraComponent->SetupAttachment(CamBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
	//-------------------------------------------------------------------------------------- volume where PlayerCharacter can sense something
	PerceptionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("SC"));
	PerceptionSphere->SetupAttachment(RootComponent);
	//-------------------------------------------------------------------------------------- Create an Inventory
	InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>(TEXT("Inventory"));
	InventoryComponent->Capacity = 20;

	//if (InventoryComponent)
	//{
	//	InventoryComponent->OnWeaponSwitch.AddDynamic(this, &ATDSCharacter::InitWeapon);
	//}
	
	//-------------------------------------------------------------------------------------- Create an Character Health Controller
	CharacterHealthComponent = CreateDefaultSubobject<UCharacterHealthComponent>(TEXT("Health"));

	if(CharacterHealthComponent)
	{
		CharacterHealthComponent->OnDeath.AddDynamic(this, &ATDSCharacter::CharDeath);
	}
	//-------------------------------------------------------------------------------------- Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (!Stand)
	{
		if (CurrentCursor)
		{
			APlayerController* PC = Cast<APlayerController>(GetController());
			if (PC)
			{
				FHitResult TraceHitResult;
				PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
				FVector CursorFV = TraceHitResult.ImpactNormal;
				FRotator CursorR = CursorFV.Rotation();

				CurrentCursor->SetWorldLocation(TraceHitResult.Location);
				CurrentCursor->SetWorldRotation(CursorR);
			}
		}
		MovementTick(DeltaSeconds);
	}
	else
	{
		FHitResult TraceHitResult;
		CurrentCursor->SetWorldLocation(TraceHitResult.Location);
	}
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	Tags.Add("Player");
	
	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}

	// Health = MaxHealth;
	CharacterHealthComponent->SetCurrentHealth(CharacterHealthComponent->GetMaxHealth());
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TrySwitchNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATDSCharacter::TrySwitchPreviousWeapon);

	NewInputComponent->BindAction(TEXT("ToggleInventory"), IE_Pressed, this, &ATDSCharacter::CallInventory);
	NewInputComponent->BindAction(TEXT("ToggleInventory"), IE_Released, this, &ATDSCharacter::ResetBotSearch);
}

void ATDSCharacter::UseItem(class UItem* Item)
{
	if (Item)
	{
		Item->Use(this);
		Item->OnUse(this); // BP Event
	}
}

void ATDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDSCharacter::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATDSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}


void ATDSCharacter::MovementTick(float deltatime)
{
	if(bIsAlive)
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FHitResult ResultingHit;
			myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultingHit);
			//myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultingHit);

			float FindRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultingHit.Location).Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindRotation, 0.0f)));

			if (CurrentWeapon)
			{
				FVector Displacement = FVector(0);
				switch (MovementState)
				{
				case EMovementState::Aim_State:
					Displacement = FVector(0.0f, 0.0f, 160.0f);
					CurrentWeapon->ShouldReduceDispersion = true;
					break;
				case EMovementState::Walk_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Run_State:
					Displacement = FVector(0.0f, 0.0f, 120.0f);
					CurrentWeapon->ShouldReduceDispersion = false;
					break;
				case EMovementState::Sprint_State:
					break;
				default:
					break;
				}

				CurrentWeapon->ShotEndLocation = ResultingHit.Location + Displacement;
				// Aim Cursor like 3d Widget?
			}
		}
	}
}

void ATDSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefaults* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		// ToDo Check melee or range
		myWeapon->SetWeaponFireState(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void ATDSCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Stand_State:
		ResSpeed = MovementSpeedInfo.StandSpeed;
		break;
	case EMovementState::Aim_State:
		ResSpeed = MovementSpeedInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementSpeedInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementSpeedInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		ResSpeed = MovementSpeedInfo.SprintSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSCharacter::ChangeMovementState()
{
	if (!Walk && !Sprint && !Aim && !Stand)
	{
		MovementState = EMovementState::Run_State;
		GetWorld()->GetTimerManager().ClearTimer(FatigueHandle);
		Fatigue = false;
	}
	else
	{
		if (Sprint && !Stand)
		{
			if (Sprint && Aim)
			{
				MovementState = EMovementState::Aim_State;
				GetWorld()->GetTimerManager().ClearTimer(FatigueHandle);
				Fatigue = false;
			}
			else
			{
				Aim = false;
				MovementState = EMovementState::Sprint_State;
				if (!Fatigue)
				{
					float inRate = Stamina;
					GetWorld()->GetTimerManager().SetTimer(FatigueHandle, this, &ATDSCharacter::OnFatigue, inRate, false);
					Fatigue = true;
				}
			}
		}
		else
		{
			if (Walk && !Aim && !Stand)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if (Aim && !Stand)
				{
					MovementState = EMovementState::Aim_State;
					GetWorld()->GetTimerManager().ClearTimer(FatigueHandle);
					Fatigue = false;
				}
				else
				{
					MovementState = EMovementState::Stand_State;
				}
			}
		}
	}
	if (CurrentWeapon)
	{
		CurrentWeapon->UpdateWeaponState(MovementState);
	}
	CharacterUpdate();
}

AWeaponDefaults* ATDSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

AWeaponDefaults* ATDSCharacter::SetCurrentWeapon(bool Gun)
{
	if (Gun == true)
	{
		CurrentWeapon = RightWeapon;
	}
	else CurrentWeapon = LeftWeapon;

	return CurrentWeapon;
}

bool ATDSCharacter::InitWeapon(FName WeaponID, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();

	if (myGI->GetWeaponInfoByName(WeaponID, myWeaponInfo))
	{
		FName GunName = "";
		if (myWeaponInfo.WeaponClass && WeaponID == "Gun")
		{
			GunName = "Gun";
		}
		else
		{
			GunName = "HeavyWeapon";
		}
		FVector SpawnLocation = GetMesh()->GetSocketLocation(GunName);
		FRotator SpawnRotation = GetMesh()->GetSocketRotation(GunName);

		AWeaponDefaults* myGun = Cast<AWeaponDefaults>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
		if (GunName == "Gun")
		{
			RightWeapon = myGun;
			CurrentWeapon = RightWeapon;
			Armed = true;
		}
		else
		{
			LeftWeapon = myGun;
			CurrentWeapon = LeftWeapon;
			HeavyArmed = true;
		}
		FAttachmentTransformRules AT_Rule(EAttachmentRule::SnapToTarget, false);
		myGun->AttachToComponent(GetMesh(), AT_Rule, FName(GunName));
		myGun->WeaponSetting = myWeaponInfo;
		myGun->WeaponInfo.Clip = myWeaponInfo.MaxClip;

		myGun->OnWeaponReloadStart.AddDynamic(this, &ATDSCharacter::WeaponReloadStart);
		myGun->OnWeaponReloadEnd.AddDynamic(this, &ATDSCharacter::WeaponReloadEnd);
		myGun->OnWeaponFireStart.AddDynamic(this, &ATDSCharacter::WeaponFireStart);

		return true;
	}
	else
	{
		return false;
	}
}

void ATDSCharacter::RemoveCurrentWeapon()
{

}

void ATDSCharacter::TryReloadWeapon()
{
	if (CurrentWeapon)
	{
		if (CurrentWeapon->GetWeaponClip() <= CurrentWeapon->WeaponSetting.MaxClip)
			CurrentWeapon->InitReload();
	}

	//if (CurrentWeapon && !CurrentWeapon->WeaponReloading)//fix reload
	//{
	//	if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
	//		CurrentWeapon->InitReload();
	//}
}

void ATDSCharacter::WeaponReloadStart(UAnimMontage* Anim, USoundBase* Sound)
{
	WeaponReloadStart_BP(Anim, Sound);
}

void ATDSCharacter::WeaponReloadEnd(bool bIsSuccessful, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentWeaponIndex, CurrentWeapon->WeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSuccessful);
}

void ATDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim, USoundBase* Sound)
{
	USkeletalMeshComponent* myMesh = GetMesh();
	USceneComponent* AddMesh = myMesh->GetChildComponent(0);
	USkeletalMeshComponent* Reloader = Cast<USkeletalMeshComponent>(AddMesh);
	UAnimInstance* AnimInstance = Reloader->GetAnimInstance();

	if (AnimInstance)
	{
		AnimInstance->Montage_Play(Anim, 1.0);
	}

	UGameplayStatics::SpawnSound2D(GetWorld(), Sound);
}

void ATDSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccessful)
{
	if(bIsSuccessful)
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("RELOADED"));
}

void ATDSCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	WeaponFireStart_BP(Anim);
}

void ATDSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	if (CurrentWeapon && CurrentWeapon->Weapon_Anim)
	{
		USkeletalMeshComponent* EquippedGun = CurrentWeapon->Weapon_Anim;
		UAnimInstance* AnimInstance = EquippedGun->GetAnimInstance();
		AnimInstance->Montage_Play(Anim, 1.0);
	}
}

UDecalComponent* ATDSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ATDSCharacter::CallInventory()
{
	if(InventoryBot)
	{	
		//-------------------------------------------------------------------------------------- Call InventoryBot & suppress Pickup Searching
		if(InventoryBot->GetBotDistance() > 160.f)
		{
			InventoryBot->EventCallBot(true);
		}
		else InventoryBot->ToggleInventory();
	}
	else
	{	
		//-------------------------------------------------------------------------------------- Search for near InventoryBot
		TArray<AActor*> OverlappingActors;
		const TSubclassOf<AInventoryBot> ClassFilter;
		PerceptionSphere->GetOverlappingActors(OverlappingActors, ClassFilter);
		float ShortestDistance = 1000.f;
		AActor* ClosestBot = nullptr;
		
		for(auto& SingleBot : OverlappingActors)
		{
			const float Distance = GetHorizontalDistanceTo(SingleBot);
			if(ShortestDistance > Distance)
			{
				ShortestDistance = Distance;
				ClosestBot = SingleBot;
			}
		}
		
		if(OverlappingActors.IsValidIndex(0))
		{	
			FTimerDelegate TimerDel;
			AInventoryBot* Bot = Cast<AInventoryBot>(ClosestBot);
			TimerDel.BindUFunction(this, "AcquireBot", Bot);
			
			GetWorldTimerManager().SetTimer(BotSearchHandle, TimerDel, BotSearchPeriod, false);
		}
		else GEngine->AddOnScreenDebugMessage(-1,1.f, FColor::Red, "NO InventoryBot");
	}
}

void ATDSCharacter::EmergencyCheck()
{
	const int ControlValue = InventoryBot->BotInventoryComponent->InventoryWidget->GetControlValue();
	if(ControlValue >= CharacterHealthComponent->GetCurrentHealth() /*Health*/)
	{
		UInventoryItemDisplay* Dummy = nullptr;
		InventoryBot->BotInventoryComponent->InventoryWidget->ReadyPickup(true, false, Dummy);
	}
}

void ATDSCharacter::OnFatigue()
{
	const float inRate = Restitution;
	Walk = true;
	Sprint = false;
	GetWorld()->GetTimerManager().SetTimer(RestHandle, this, &ATDSCharacter::OnRest, inRate, false);
}

void ATDSCharacter::OnRest()
{
	Walk = false;
	Fatigue = false;
}

EPhysicalSurface ATDSCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = SurfaceType_Default;
	if (CharacterHealthComponent)
	{
		// if (CharacterHealthComponent->GetCurrentShield() <= 0)
		// {
			if (GetMesh())
			{
				UMaterialInterface* Material = GetMesh()->GetMaterial(0);
				if (Material)
				{
					Result = Material->GetPhysicalMaterial()->SurfaceType;
				}
			}
		// }		
	}			
	return Result;
}

TArray<UStateEffect*> ATDSCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ATDSCharacter::RemoveEffect(UStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATDSCharacter::AddEffect(UStateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}

void ATDSCharacter::CharDeath()
{
	float AnimTime = 0.0f;
	const int32 RandomDeath = FMath::RandHelper(DeathAnim.Num());

	if(DeathAnim.IsValidIndex(RandomDeath)&&DeathAnim[RandomDeath]&&GetMesh()&&GetMesh()->GetAnimInstance())
	{
		AnimTime = DeathAnim[RandomDeath]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeathAnim[RandomDeath]);
	}

	bIsAlive = false;
	UnPossessed();
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	//-------------------------------------------------------------------------------------- RagDoll Timer
	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer,this, &ATDSCharacter::EnableRagDoll, AnimTime, false);	
	GetCursorToWorld()->SetVisibility(false);
}

void ATDSCharacter::EnableRagDoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}	
}

// float ATDSCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
// {
// 	const float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
//
// 	if(bIsAlive)
// 	{
// 		CharacterHealthComponent->ChangeHealthValue(-DamageAmount);
// 	}
//
// 	if(DamageEvent.IsOfType(0))
// 	{
// 		AProjectileDefaults* Projectile = Cast<AProjectileDefaults>(DamageCauser);
// 		if (Projectile)
// 		{
// 			UTypes::AddEffectBySurfaceType(this, Projectile->ProjectileSettings.Effect, GetSurfaceType());			
// 		}
// 	}
// 	
// 	return ActualDamage;
// }

//ToDO in one func TrySwitchPreviousWeapon && TrySwitchNextWeapon
//need Timer to Switch with Anim, this method stupid i must know switch success for second logic inventory
//now we not have not success switch/ if 1 weapon switch to self
void ATDSCharacter::TrySwitchNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentWeaponIndex;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->WeaponSwitchToIndex(CurrentWeaponIndex + 1, OldIndex, OldInfo, true))
			{
			}
		}
	}
}

void ATDSCharacter::TrySwitchPreviousWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentWeaponIndex;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (InventoryComponent->WeaponSwitchToIndex(CurrentWeaponIndex - 1, OldIndex, OldInfo, false))
			{

			}
		}
	}
}

void ATDSCharacter::ResetBotSearch()
{
	GetWorldTimerManager().ClearTimer(BotSearchHandle);
}

void ATDSCharacter::AcquireBot(AInventoryBot* Bot)
{
	if(Bot->GetBotID() == 0)
	{
		UTDSGameInstance* GameInstance = Cast<UTDSGameInstance>(GetGameInstance());
		GameInstance->SetBotCatalog();
		Bot->SetBotID(GameInstance->GetBotCatalog());
	}

	Bot->BotInventoryComponent->InventoryWidget->ClearWidgets(UInventoryWidget::StaticClass());
	
	InventoryBot = Bot;
	InventoryBot->SpawnDefaultController();
	InventoryBot->BotInventoryComponent->InitializeInventory();
	InventoryBot->StartScanTimer();
	InventoryBot->SetActorTickEnabled(true);
	InventoryBot->GetController()->SetActorTickEnabled(true);
	InventoryBot->SetAvailability(true); // Launches Distances Check

	InventoryBot->SetOwningCharacter(this);
	InventoryBot->StateCheck();
}
