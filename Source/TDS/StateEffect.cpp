﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "StateEffect.h"
#include "HealthComponent.h"
#include "Interface/TDS_IGameActor.h"
#include "Kismet/GameplayStatics.h"

bool UStateEffect::InitObject(FHitResult Hit)
{
	HitResult = Hit;
	AActor* TestActor = Cast<AActor>(Hit.Actor);
	MyActor = TestActor;
	ITDS_IGameActor* Interface = Cast<ITDS_IGameActor>(MyActor);

	if(Interface)
	{
		Interface->AddEffect(this);
	}

	if (MyActor->GetClass()->ImplementsInterface(UTDS_IGameActor::StaticClass()))
	{
		ITDS_IGameActor::Execute_EffectLauncher(MyActor, Hit, this);
	}

	return true;
}

void UStateEffect::DestroyObject()
{
	ITDS_IGameActor* Interface = Cast<ITDS_IGameActor>(MyActor);

	if(Interface)
	{
		Interface->RemoveEffect(this);
	}

	MyActor = nullptr;
	
	if(this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}	
}

void UStateEffect::ApplyEffect_Implementation(FHitResult Target)
{
}

bool UStateEffect_ExecuteOnce::InitObject(FHitResult Hit)
{
	Super::InitObject(Hit);
	ExecuteOnce();

	return true;
}

void UStateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UStateEffect_ExecuteOnce::ExecuteOnce()
{
	if(MyActor)
	{
		UHealthComponent* HealthComp = Cast<UHealthComponent>(MyActor->GetComponentByClass(UHealthComponent::StaticClass()));
		
		if(HealthComp)
		{
			HealthComp->ChangeHealthValue(Power);
		}
	}	

	ApplyEffect(HitResult);
	
	DestroyObject();
}

bool UStateEffect_ExecuteTimer::InitObject(FHitResult Hit)
{
	Super::InitObject(Hit);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UStateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UStateEffect_ExecuteTimer::Execute, TimeRate, true);
	
	if(ParticleEffect)
	{
		FVector SpawnLocation = Hit.Location;
		USceneComponent* SceneComponent = MyActor->GetRootComponent();
		
		if(USkeletalMeshComponent* MySkeletalMesh = Cast<USkeletalMeshComponent>(Hit.Component))
		{
			SceneComponent = MySkeletalMesh;
			SpawnLocation = MySkeletalMesh->GetBoneLocation(Hit.BoneName);
		}
		 
		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, SceneComponent, Hit.BoneName, SpawnLocation, FRotator::ZeroRotator,
														EAttachLocation::KeepWorldPosition,false);
	}
	
	return true;
}

void UStateEffect_ExecuteTimer::DestroyObject()
{
	if(ParticleEffect)
	{
		ParticleEmitter->DestroyComponent();
		ParticleEmitter = nullptr;
	}

	Super::DestroyObject();
}

void UStateEffect_ExecuteTimer::Execute()
{
	if(MyActor)
	{	
		UHealthComponent* HealthComp = Cast<UHealthComponent>(MyActor->GetComponentByClass(UHealthComponent::StaticClass()));
		if(HealthComp)
		{
			HealthComp->ChangeHealthValue(Power);
		}
	}

	ApplyEffect(HitResult);
}

