﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "InventoryWidget.h"

#include "ItemActionMenu.h"
#include "TDS/TDSCharacter.h"
#include "Components/Border.h"
#include "Components/SpinBox.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "InventorySlot.h"
#include "Misc/OutputDeviceNull.h"

void UInventoryWidget::NativeConstruct()
{
	Super::NativeConstruct();

	// bIsFocusable = true;

	WidgetButtons = {ForwardButton, BackwardButton, LeftButton, RightButton};
	SlotNumbers = {F_Text, B_Text, L_Text, R_Text};
	//-------------------------------------------------------------------------------------- Setting right Values for the CheckBoxes and Spinner of the Widget
	ATDSCharacter* Character = Cast<ATDSCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());

	ReactionSpinner->SetMaxSliderValue(Character->MaxHealth - 10.f);
	
	if(Collector)
	{
		
		if(Character->InventoryBot->IsCollector)
		{
			Collector->SetCheckedState(ECheckBoxState::Checked);
		}
		else Collector->SetCheckedState(ECheckBoxState::Unchecked);

		if(Character->InventoryBot->IsAutomatic)
		{
			Automatic->SetCheckedState(ECheckBoxState::Checked);
		}
		else Automatic->SetCheckedState(ECheckBoxState::Unchecked);
	}
}

void UInventoryWidget::WidgetActivation(bool IsActive)
{
	if(IsActive)
	{
		ReactionSpinner->OnValueCommitted.AddDynamic(this, &UInventoryWidget::SpinnerValueCommitted);
		ForwardButton->OnClicked.AddDynamic(this, &UInventoryWidget::OnClickForwardButton);
		BackwardButton->OnClicked.AddDynamic(this, &UInventoryWidget::OnClickBackwardButton);
		LeftButton->OnClicked.AddDynamic(this, &UInventoryWidget::OnClickLeftButton);
		RightButton->OnClicked.AddDynamic(this, &UInventoryWidget::OnClickRightButton);
		Deactivation->OnClicked.AddDynamic(this, &UInventoryWidget::DeactivationSignal);
		if(Collector) Collector->OnCheckStateChanged.AddDynamic(this, &UInventoryWidget::BotStatus);
		if(Automatic) Automatic->OnCheckStateChanged.AddDynamic(this, &UInventoryWidget::AutomaticBot);
	}
	else
	{
		ReactionSpinner->OnValueCommitted.RemoveDynamic(this, &UInventoryWidget::SpinnerValueCommitted);
		ForwardButton->OnClicked.RemoveDynamic(this, &UInventoryWidget::OnClickForwardButton);
		BackwardButton->OnClicked.RemoveDynamic(this, &UInventoryWidget::OnClickBackwardButton);
		LeftButton->OnClicked.RemoveDynamic(this, &UInventoryWidget::OnClickLeftButton);
		RightButton->OnClicked.RemoveDynamic(this, &UInventoryWidget::OnClickRightButton);
		Deactivation->OnClicked.RemoveDynamic(this, &UInventoryWidget::DeactivationSignal);
		if(Collector) Collector->OnCheckStateChanged.RemoveDynamic(this, &UInventoryWidget::BotStatus);
		if(Automatic) Automatic->OnCheckStateChanged.RemoveDynamic(this, &UInventoryWidget::AutomaticBot);
	}
}

void UInventoryWidget::SpinnerValueCommitted(float EnteredValue, ETextCommit::Type MyType)
{
	ControlValue = FMath::FloorToInt(round(EnteredValue));
}

FHitResult UInventoryWidget::VolumeCheck(FVector Start, FVector End)
{
	FHitResult OutHit;
	const TArray<AActor*> ActorsToIgnore;
	TArray<TEnumAsByte<EObjectTypeQuery>> TraceObjectTypes;
	TraceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_GameTraceChannel3));
	
	UKismetSystemLibrary::SphereTraceSingleForObjects(this, Start, End, 32, TraceObjectTypes, false, ActorsToIgnore, EDrawDebugTrace::None,
													OutHit, true);
	return OutHit;
}

bool UInventoryWidget::SpecifySpawnPoints(UInventoryItemDisplay* Item, int Index)
{
	AInventoryBot* InventoryBot = Cast<AInventoryBot>(InventoryGridPanel->GetInventoryComponent()->GetOwner());
	FCollisionObjectQueryParams Objects = ECC_WorldStatic;
	FRotator SpawnRotation = {0, 0, 0};
	FActorSpawnParameters SpawnParameters;
	FVector Start;
	//-------------------------------------------------------------------------------------- Getting Data for LineTrace
	if(Index <= 1)
	{
		Start = InventoryBot->GetActorLocation() + InventoryBot->GetActorForwardVector() * InventoryBot->GetSpawnPoints()[Index];
	}
	else Start = InventoryBot->GetActorLocation() + InventoryBot->GetActorRightVector() * InventoryBot->GetSpawnPoints()[Index];

	FVector DownVector = {0.f, 0.f, -200.f};
	FVector End = Start + DownVector;
	//-------------------------------------------------------------------------------------- Trace
	FHitResult LineTrace = InventoryBot->TraceCheck(Start, End, Objects);
							
	if(LineTrace.bBlockingHit)
	{
		FHitResult SphereTrace = VolumeCheck(LineTrace.Location, LineTrace.Location);
		
		if(!SphereTrace.bBlockingHit)
		{
			//-------------------------------------------------------------------------------------- Spawn Pickup at LineTrace Intersection Location
			InventoryBot->Pickup_Scanner->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			SpawnPickup(LineTrace.Location, SpawnRotation, SpawnParameters, Item);
			InventoryBot->Pickup_Scanner->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

			return true;
		}
		//-------------------------------------------------------------------------------------- Checking another Pickup that already in the Spawn Position
		APickupBase* Pickup = Cast<APickupBase>(SphereTrace.Actor);
		FString TracedPickup = Pickup->ItemData->GetItemName().ToString();
		FString DroppedItem = Item->ItemData->GetItemName().ToString();

		if(TracedPickup == DroppedItem)
		{
			//-------------------------------------------------------------------------------------- Adding Item to the Stack of Pickup (if same)
			int Iterator = 1;
			if(FullStack) {Iterator = Item->GetItemCount();}
			
			for(int i = Iterator; i > 0; i--)
			{
				Pickup->ItemCounter->SetText(FText::AsNumber(Pickup->SetPickupStack(1)));
				Pickup->ItemCounter->SetVisibility(true);
				Item->ChangeItemCount(-1);
				InventoryGridPanel->GetInventoryComponent()->ChangeInventoryVolume(-Item->ItemData->GetItemVolume());
				VolumeBarState();
			}

			if(Item->GetItemCount() == 0)
			{
				InventoryGridPanel->RemoveItem(Item->ItemData);
			}
			return true;
		}
	}
	return false;
}

void UInventoryWidget::OnClickForwardButton()
{
	ActivatingSlot(0);
}

void UInventoryWidget::OnClickBackwardButton()
{
	ActivatingSlot(1);
}

void UInventoryWidget::OnClickLeftButton()
{
	ActivatingSlot(2);
}

void UInventoryWidget::OnClickRightButton()
{
	ActivatingSlot(3);
}

void UInventoryWidget::ReadyPickup(bool Emergency, bool Reconstruct, UInventoryItemDisplay* Item)
{
	TArray<UInventoryItemDisplay*> ItemsToDel;
	
	if(Emergency)
	{
		for(auto& EmergencyItem : InventoryGridPanel->ItemDisplayContainer)
		{
			TMap<UInventorySlot*, FButtonStruct> Slots;
			if(Reconstruct) {Slots = AdditionalSlots;}
			else Slots = DropSlots;

			for(auto& SlotToCheck : Slots)
			{
				if(SlotToCheck.Key->GetIndex() == EmergencyItem->GetInventoryIndex())
				{
					int Index = 0;
					for(auto& Button : SlotToCheck.Value.ActiveButtons)
					{
						if(Button)
						{
							if(EmergencyItem->GetItemCount() > 0)
							{
								SpecifySpawnPoints(EmergencyItem, Index); // Index = Position around InventoryBot
							}
							if(EmergencyItem->GetItemCount() == 0)
							{
								ItemsToDel.AddUnique(EmergencyItem);
							}
						}
						Index++;
					}
				}
			}
		}
	}
	else
	{
		for(int i = 0; i < 4; i++)
		{
			if(SpecifySpawnPoints(Item, i)) // i = Position around InventoryBot
			{
				if(Item->GetItemCount() == 0)
				{
					ItemsToDel.AddUnique(Item);
				}	
				i = 4;
			}
		}
	}
	
	for(int i = ItemsToDel.Num()-1; i > -1; i--)
	{
		InventoryGridPanel->RemoveItem(ItemsToDel[i]->ItemData);
	}
	
	if(!Reconstruct) InventoryGridPanel->ReconstructInventoryGrid();
}

void UInventoryWidget::SpawnPickup(FVector SpawnLocation, FRotator SpawnRotation, FActorSpawnParameters SpawnParameters, UInventoryItemDisplay* Item)
{
	APickupBase* SpawnedPickup = GetWorld()->SpawnActor<APickupBase>(SpawnLocation, SpawnRotation, SpawnParameters);
	UPickupDABase* NewDataAsset = DuplicateObject(Item->ItemData, SpawnedPickup);

	if(FullStack)
	{
		SpawnedPickup->SetPickupStack(Item->GetItemCount() - 1);
		SpawnedPickup->ItemCounter->SetText(FText::AsNumber(SpawnedPickup->GetPickupStack()));
	
		if(SpawnedPickup->GetPickupStack() > 1)
		{
			SpawnedPickup->ItemCounter->SetVisibility(true);
		}
	}
	
	SpawnedPickup->ItemData = NewDataAsset;
	SpawnedPickup->PickupData = Item->PickupData;
	SpawnedPickup->SetCollectable(false);
	SpawnedPickup->InitItemActor();

	int CheckValue;
	if(FullStack) {CheckValue = Item->GetItemCount();}
	else CheckValue = 1;

	for(int i = CheckValue; i > 0; i--)
	{
		Item->ChangeItemCount(-1);
		InventoryGridPanel->GetInventoryComponent()->ChangeInventoryVolume(-Item->ItemData->GetItemVolume());
		VolumeBarState();
	}
}

void UInventoryWidget::ActivatingSlot(int ButtonIndex)
{
	FButtonStruct* ButtonSet = DropSlots.Find(InventoryGridPanel->SelectedSlot);

	if(ButtonSet == nullptr)
	{
		DropButtons.ActiveButtons[ButtonIndex] = true;
		DropSlots.Add(InventoryGridPanel->SelectedSlot, DropButtons);
	}
	else
	{
		DropButtons = *ButtonSet;
		DropButtons.ActiveButtons[ButtonIndex] = true;
		DropSlots.Add(InventoryGridPanel->SelectedSlot, DropButtons);
	}
	
	if(InventoryGridPanel->SelectedSlot)
	{
		InventoryGridPanel->SelectedSlot->SlotBorder->SetBrushColor(InventoryGridPanel->SelectionColor.ActivatedSlotColor);
		DropButtons.ActiveButtons = {0, 0, 0, 0};
	}

	SlotNumbers[ButtonIndex]->Text = FText::FromString(FString::FromInt(InventoryGridPanel->SelectedSlot->GetIndex() + 1));
	SlotNumbers[ButtonIndex]->SynchronizeProperties();
	SlotNumbers[ButtonIndex]->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
	WidgetButtons[ButtonIndex]->SetIsEnabled(false);
}

FReply UInventoryWidget::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	FEventReply Reply;
	Reply.NativeReply = Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);

	for(auto& Test : InventoryGridPanel->SlotMap)
	{
		FString MyString = FString::FromInt(Test.Value);
		GEngine->AddOnScreenDebugMessage(-1,2.f, FColor::Cyan, MyString);

	}
	
	AInventoryBot* InventoryBot = Cast<AInventoryBot>(InventoryGridPanel->GetInventoryComponent()->GetOwner());
	InventoryBot->HideActionMenu();
	
	return Reply.NativeReply;
}

FReply UInventoryWidget::NativeOnMouseButtonUp(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	return FReply::Handled();
}

void UInventoryWidget::VolumeBarState()
{
	const int Volume = InventoryGridPanel->GetInventoryComponent()->GetInventoryVolume();
	const int MaxVolume = InventoryGridPanel->GetInventoryComponent()->GetInventoryMaxVolume();
	const float OverallVolume = float(Volume) / float(MaxVolume);
	const FString VolumeString =  FString::FromInt(Volume) + " / " + FString::FromInt(MaxVolume);
	VolumeBar->SetPercent(OverallVolume);
	VolumeRate->SetText(FText::AsCultureInvariant(VolumeString));
}

void UInventoryWidget::BotStatus(const bool IsChecked)
{
	ATDSCharacter* Character = Cast<ATDSCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	AInventoryBot* Bot = Cast<AInventoryBot>(GetWorld()->GetFirstPlayerController()->GetPawn());
	
	if(Character)
	{
		Character->InventoryBot->IsCollector = IsChecked;
	
		if(IsChecked) Character->InventoryBot->GetCapsuleComponent()->ComponentTags.Add("Collector");
		else Character->InventoryBot->GetCapsuleComponent()->ComponentTags.Remove("Collector");
	}
	else if(Bot)
	{
		Bot->IsCollector = IsChecked;
	
		if(IsChecked) Bot->GetCapsuleComponent()->ComponentTags.Add("Collector");
		else Bot->GetCapsuleComponent()->ComponentTags.Remove("Collector");
	}
}

void UInventoryWidget::AutomaticBot(const bool IsChecked)
{
	ATDSCharacter* Character = Cast<ATDSCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	AInventoryBot* Bot = Cast<AInventoryBot>(GetWorld()->GetFirstPlayerController()->GetPawn());

	if(Character)
	{
		Character->InventoryBot->IsAutomatic = IsChecked;
	}
	else if(Bot)
	{
		Bot->IsAutomatic = IsChecked;
	}
}

void UInventoryWidget::DeactivationSignal()
{
	if(ATDSCharacter* Character = Cast<ATDSCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn()))
	{
		Character->InventoryBot->ToggleInventory();
		Character->InventoryBot->DeactivateBot();
	}
	else if(AInventoryBot* Bot = Cast<AInventoryBot>(GetWorld()->GetFirstPlayerController()->GetPawn()))
	{
		Bot->ToggleInventory();

		FOutputDeviceNull Ar;
		GetWorld()->GetFirstPlayerController()->CallFunctionByNameWithArguments(TEXT("DeactivateBot"), Ar, nullptr, true);
		
		Bot->DeactivateBot();
	}
}

void UInventoryWidget::ClearWidgets(const TSubclassOf<class UUserWidget> AnotherClass)
{
	//-------------------------------------------------------------------------------------- Hide ItemActionMenu
	TArray<UUserWidget*> Menu;
	UWidgetBlueprintLibrary::GetAllWidgetsOfClass(this, Menu, UItemActionMenu::StaticClass(), false);

	if(Menu.Num() != 0)
	{
		UItemActionMenu* ActionMenu = Cast<UItemActionMenu>(Menu[0]);
		ActionMenu->SetVisibility(ESlateVisibility::Hidden);
	}
	//-------------------------------------------------------------------------------------- Destroy all depreciated Widgets 
	TArray<UUserWidget*> Items;
	UWidgetBlueprintLibrary::GetAllWidgetsOfClass(this, Items, AnotherClass, false);
	
	for(auto& SingleItem : Items)
	{
		if(!SingleItem->IsVisible())
		{
			SingleItem->ConditionalBeginDestroy();
			SingleItem = nullptr;
		}
	}
}

void UInventoryWidget::Enhancement_Implementation(const TMap<UPickupDABase*, bool>& Item)
{
	// All Functionality should be created in a BP
}

void UInventoryWidget::Modification_Implementation(const TMap<UPickupDABase*, bool>& Item)
{
	// All Functionality should be created in a BP
}

void UInventoryWidget::SwapGrids(UInventoryItemDisplay* Item, UInventorySlot* ActualSlot)
{
	for(auto& Grid : InventoryGrids)
	{
		if(Grid.Value == true)
		{
			Grid.Key->AddItem(Item->ItemData, Item->PickupData, ActualSlot->GetIndex());
		}
		else Grid.Key->RemoveItem(Item->ItemData);
	}
}
