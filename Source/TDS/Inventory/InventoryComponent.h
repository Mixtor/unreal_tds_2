﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDS/Pickup/PickupBase.h"

#include "InventoryComponent.generated.h"

class UPickupDABase;
class UInventoryWidget;
class UDynamicInventoryGrid;
class UInventoryItemDisplay;

UCLASS(BlueprintType, Blueprintable, ClassGroup = "Inventory")
class TDS_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UInventoryComponent();
	
	UPROPERTY(/*EditDefaultsOnly, */EditAnywhere, Category = "UI")
		TSubclassOf<UInventoryWidget> InventoryWidgetClass;
	
	UPROPERTY(BlueprintReadOnly)
		UInventoryWidget* InventoryWidget = nullptr;
	
	UPROPERTY()
		TArray<const UPickupDABase*> ItemContainer;

	UFUNCTION(BlueprintCallable)
		void InitializeInventory();
	
	UFUNCTION(BlueprintCallable)
		int GetInventoryVolume() const {return InventoryVolume;}
	UFUNCTION(BlueprintCallable)
		int GetInventoryMaxVolume() const {return InventoryMaxVolume;}
	UFUNCTION(BlueprintCallable)
		void SetInventoryVolume(const int Volume) {InventoryVolume = Volume;}
	UFUNCTION(BlueprintCallable)
		void SetInventoryMaxVolume(const int Volume) {InventoryMaxVolume = Volume;}
	UFUNCTION(BlueprintCallable)
		void ChangeInventoryVolume(int Volume);

	UFUNCTION(BlueprintCallable)
		bool PickUpItem(APickupBase* Pickup);
	UFUNCTION(BlueprintCallable)
		void RemoveItem(UPickupDABase* ItemData);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:

	UPROPERTY()
		int InventoryInitialVolume = 100;
	UPROPERTY()
		int InventoryMaxVolume;	
	UPROPERTY()
		int InventoryVolume;
	
/*public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;*/
};
