﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "InventoryItemDisplay.h"
//#include "InventorySlot.h"
#include "ItemInfo.h"
#include "Blueprint/UserWidget.h"
#include "Components/CheckBox.h"
#include "Components/ProgressBar.h"
#include "TDS/Inventory/AdvancedButton.h"

#include "InventoryWidget.generated.h"

class UDynamicInventoryGrid;
class USpinBox;
/**
* 
*/
USTRUCT()
struct FButtonStruct
{
	GENERATED_BODY()

	UPROPERTY()
		TArray<bool> ActiveButtons = {0, 0, 0, 0};
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCheckBoxCollectorChange, bool, bIsChecked);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCheckBoxAutoChange, bool, bIsChecked);

UCLASS()
class TDS_API UInventoryWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnCheckBoxCollectorChange OnCheckBoxCollectorChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnCheckBoxAutoChange OnCheckBoxAutoChange;
	
	UPROPERTY()
		TMap<UInventorySlot*, FButtonStruct> DropSlots;
	UPROPERTY()
		TMap<UInventorySlot*, FButtonStruct> AdditionalSlots;
	
	UPROPERTY()
		FButtonStruct DropButtons;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
		UDynamicInventoryGrid* InventoryGridPanel = nullptr;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget, OptionalWidget = true))
		UDynamicInventoryGrid* ModifierGridPanel = nullptr;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget, OptionalWidget = true))
		UCheckBox* Collector = nullptr;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget, OptionalWidget = true))
		UCheckBox* Automatic = nullptr;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget, OptionalWidget = true))
		UButton* Deactivation = nullptr;
	
	UPROPERTY()
		TMap<UDynamicInventoryGrid*, bool> InventoryGrids; 

	UPROPERTY()
		TMap<UInventoryItemDisplay*, int> SwappedItems;
	
	UPROPERTY(meta = (BindWidget))
		UItemInfo* ItemInfo = nullptr;
		
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
		USpinBox* ReactionSpinner = nullptr;
	UPROPERTY(meta = (BindWidget))
		UProgressBar* VolumeBar = nullptr;
	UPROPERTY(meta = (BindWidget))
        UTextBlock* VolumeRate = nullptr;
	
	UPROPERTY(meta = (BindWidget))
        UAdvancedButton* ForwardButton = nullptr;
	UPROPERTY(meta = (BindWidget))
		UAdvancedButton* BackwardButton = nullptr;
	UPROPERTY(meta = (BindWidget))
		UAdvancedButton* LeftButton = nullptr;
	UPROPERTY(meta = (BindWidget))
		UAdvancedButton* RightButton = nullptr;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* F_Text = nullptr;
	UPROPERTY(meta = (BindWidget))
		UTextBlock* B_Text = nullptr;
	UPROPERTY(meta = (BindWidget))
		UTextBlock* L_Text = nullptr;
	UPROPERTY(meta = (BindWidget))
		UTextBlock* R_Text = nullptr;
	
	UPROPERTY(BlueprintReadOnly)
		TArray<UAdvancedButton*> WidgetButtons;
	UPROPERTY()
		TArray<UTextBlock*> SlotNumbers; 
	
	UFUNCTION()
		void SwapGrids(UInventoryItemDisplay* Item, UInventorySlot* ActualSlot);

	UFUNCTION()
		void WidgetActivation(bool IsActive);
	
	UFUNCTION()
		void OnClickForwardButton();
	UFUNCTION()
		void OnClickBackwardButton();
	UFUNCTION()
		void OnClickLeftButton();
	UFUNCTION()
		void OnClickRightButton();

	UFUNCTION(BlueprintCallable)
		void ReadyPickup(bool Emergency, bool Reconstruct, UInventoryItemDisplay* Item);

	void SpawnPickup(FVector SpawnLocation, FRotator SpawnRotation, FActorSpawnParameters SpawnParameters, UInventoryItemDisplay* Item);

	UFUNCTION(BlueprintCallable)
		void VolumeBarState();

	int GetControlValue() const {return ControlValue;}

	void SetStack(const bool Stack) {FullStack = Stack;}
	
	UFUNCTION()
		void ClearWidgets(TSubclassOf<class UUserWidget> AnotherClass);
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Inventory")
		void Enhancement (const TMap<UPickupDABase*, bool>& Item);
		void Enhancement_Implementation (const TMap<UPickupDABase*, bool>& Item);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Inventory")
		void Modification (const TMap<UPickupDABase*, bool>& Item);
		void Modification_Implementation (const TMap<UPickupDABase*, bool>& Item);
	
private:
	
	bool FullStack = false;
	
	int32 ControlValue;

	UFUNCTION()
		void BotStatus(bool IsChecked);
	UFUNCTION()
		void AutomaticBot(bool IsChecked);
	UFUNCTION()
		void DeactivationSignal();
	UFUNCTION()
		void SpinnerValueCommitted(float EnteredValue, ETextCommit::Type MyType);

	FHitResult VolumeCheck(FVector Start, FVector End);
	
	bool SpecifySpawnPoints(UInventoryItemDisplay* Item, int Index);
	
	void ActivatingSlot(int ButtonIndex);
	
	virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	virtual FReply NativeOnMouseButtonUp(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

protected:
	
	virtual void NativeConstruct() override;
};




