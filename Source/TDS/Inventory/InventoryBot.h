﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "AIController.h"
#include "InventoryComponent.h"
#include "ItemActionMenu.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/InputSettings.h"

#include "InventoryBot.generated.h"

class ATDSCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCommand, bool, Command);

USTRUCT()
struct FItemInfoStruct
{
	GENERATED_BODY()

	FText ItemName = FText(FText::FromString("Item"));
	FString Description = "Description";
	int ItemVolume = 0;
	int OverallItems = 0;
};

UCLASS()
class TDS_API AInventoryBot : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AInventoryBot();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Inventory)
		UStaticMeshComponent* BotMesh = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Inventory)
		USphereComponent* Pickup_Scanner = nullptr;
	UPROPERTY(BlueprintReadOnly, Category = Inventory)
		UArrowComponent* Slots = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory)
		UInventoryComponent* BotInventoryComponent;

	UPROPERTY(BlueprintReadOnly, Category = Inventory)
		TArray<FKey> InputKeys;
	UPROPERTY()
		UItemActionMenu* ItemActionMenu;
	UPROPERTY()
		TSubclassOf<UItemActionMenu> ActionUIClass;
	
	UPROPERTY(BlueprintReadWrite, Category = Inventory)
		bool GroundCheck = false;
	UPROPERTY(BlueprintReadWrite, Category = Inventory)
		bool IsGround = true;
	
	UPROPERTY(BlueprintReadOnly, Category = Inventory)
		bool IsInventoryActive = false;

	UPROPERTY(EditAnywhere, Category = Pickups)
		bool IsCollector = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pickups)
		bool IsAutomatic = false;

	UPROPERTY()
		FOnCommand OnCommand; 
	
	bool bActionMenu = false;

	UFUNCTION(BlueprintCallable)
		int GetBotID() const {return BotID;}
	UFUNCTION(BlueprintCallable)
		void SetBotID(const int IdNumber) {BotID = IdNumber;}
	
	UFUNCTION(BlueprintCallable)
		bool ToggleInventory();

	UFUNCTION(BlueprintCallable)
		void CheckAvailablePickups() const;

	UFUNCTION(BlueprintCallable)
		void StartScanTimer();

	UFUNCTION()
		void HideActionMenu() const;

	UFUNCTION()
		void InventoryInfo(const FItemInfoStruct ItemInfoStruct) const;

	UFUNCTION(BlueprintCallable)
		void DeactivateBot();

	UFUNCTION(BlueprintCallable)
		float GetDeactivationDistance() const {return DeactivationDistance;}
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void StateCheck();
		void StateCheck_Implementation();

	UFUNCTION(BlueprintCallable)
		ATDSCharacter* GetOwningCharacter() const {return OwningCharacter;}

	void SetOwningCharacter(ATDSCharacter* Character) {OwningCharacter = Character;}

	void SetAvailability(const bool Availability) {Available = Availability;}

	void EnableUIMode(const bool IsEnable) const;

	void SetCallStatus(const bool Status) {IsCalling = Status;}
	void EventCallBot(const bool Calling);
	
	void LaunchScanner() const;

	float GetBotDistance() const {return BotDistance;}

	
	TArray<float> GetSpawnPoints() const {return SpawnPoints;} 
	
	FHitResult TraceCheck(FVector Start, FVector End, FCollisionObjectQueryParams Objects);
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	UPROPERTY(EditAnywhere, Category = DistanceLimits)
		float ReturnDistance = 1000.f;
	UPROPERTY(EditAnywhere, Category = DistanceLimits)
		float DeactivationDistance = 1500.f;
	
	UPROPERTY(EditAnywhere, Category = Inventory)
		float SpawnDistance = 100.f;
	
	UPROPERTY(EditAnywhere, Category = PickupScanner)
		float ScanPeriod = 1.5f;

	UPROPERTY(EditAnywhere, Category = CollisionScanner)
		float ScanDistance = 300.f;
	UPROPERTY(EditAnywhere, Category = CollisionScanner)
		float ScanDepth = 200.f;

	bool IsCalling = false;
	bool Return = false;

	bool Available = false;
	
	int BotID = 0;
	
	float BotDistance;

	TArray<float> SpawnPoints;

	float MaxScanRadius;
	float ClosestDistance;
	FTimerHandle ScanTimerHandle;

	UPROPERTY()
		ATDSCharacter* OwningCharacter;
	
	void ScanProcedure(float DeltaTime);
	
	UFUNCTION()
		void OverlapCheck(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
							const FHitResult& SweepResult);

	float MeasureBotDistance() const;
	void BotDistanceConditions(const float Distance);
	
	void SetInputKeysArray();
	void SetSpawnPoints();
	void ControllingOverlaps(bool Collector, bool Scanner) const;
	bool InventoryCheck(AActor* Pickup) const;
};
