﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "InventoryItemDisplay.h"

#include "DragDropWidget.h"
#include "InventoryWidget.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Components/Border.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/Image.h"
#include "ItemActionMenu.h"
#include "Kismet/GameplayStatics.h"
#include "TDS/TDSCharacter.h"

void UInventoryItemDisplay::NativeConstruct()
{
	Super::NativeConstruct();
	//-------------------------------------------------------------------------------------- Setting Variable WorkGrid
	if(ATDSCharacter* InventoryBotOwner = Cast<ATDSCharacter>(GetOwningPlayerPawn()))
	{
		InventoryBot = InventoryBotOwner->InventoryBot;
	}
	else if(AInventoryBot* ControlledBot = Cast<AInventoryBot>(GetOwningPlayerPawn()))
	{
		InventoryBot = ControlledBot;
		GEngine->AddOnScreenDebugMessage(-1,2.f, FColor::Red, "ELSE");
	}
	WorkGrid = InventoryBot->BotInventoryComponent->InventoryWidget->InventoryGridPanel;
	
	//-------------------------------------------------------------------------------------- Changing Item Icon Size for ModifierGridPanel
	if(Owner)
	{
		if(Owner->IsModifierGrid)
		{
			UCanvasPanelSlot* SlotCanvas = Cast<UCanvasPanelSlot>(SlotBorder->Slot);
			const FVector2D ActualSize = SlotCanvas->GetSize();
			SlotCanvas->SetSize({ActualSize.X - 100, ActualSize.Y - 100});
		}
	}
	//-------------------------------------------------------------------------------------- Setting Border initial Color for Item Icon
	if(SlotBorder)
	{
		BorderInitialColor = SlotBorder->BrushColor;	
	}
}

FItemInfoStruct UInventoryItemDisplay::GeneratingInfo(bool OverItem)
{
	if(OverItem)
	{
		ItemInfoStruct.ItemName = this->ItemData->GetItemName();
		ItemInfoStruct.Description = this->ItemData->GetDescription();
		ItemInfoStruct.ItemVolume = this->ItemData->GetItemVolume();
		ItemInfoStruct.OverallItems = GetItemCount();
	}
	else
	{
		ItemInfoStruct.ItemName = FText::FromString("Item");
		ItemInfoStruct.Description = "Description";
		ItemInfoStruct.ItemVolume = 0;
		ItemInfoStruct.OverallItems = 0;
	}
	return ItemInfoStruct;
}

void UInventoryItemDisplay::SetPossibleActions()
{
	if(ItemData->GetPickupType() != EIT_Item)
	{
		UTextBlock* Title = Cast<UTextBlock>(InventoryBot->ItemActionMenu->EquipButton->GetChildAt(0));
		
		InventoryBot->ItemActionMenu->DropButton->SetVisibility(ESlateVisibility::Visible);
		InventoryBot->ItemActionMenu->DropAllButton->SetVisibility(ESlateVisibility::Visible);
		
		if(Owner->IsModifierGrid)
		{
			Title->SetText(FText::FromString(TEXT("Unequip")));
			InventoryBot->ItemActionMenu->DropButton->SetVisibility(ESlateVisibility::Collapsed);
			InventoryBot->ItemActionMenu->DropAllButton->SetVisibility(ESlateVisibility::Collapsed);
		}
		else Title->SetText(FText::FromString(TEXT("Equip")));
		
		InventoryBot->ItemActionMenu->EquipButton->SetVisibility(ESlateVisibility::Visible);
		InventoryBot->ItemActionMenu->UseButton->SetVisibility(ESlateVisibility::Collapsed);

		InventoryBot->ItemActionMenu->SetActiveItem(this);
	}
	else
	{
		InventoryBot->ItemActionMenu->UseButton->SetVisibility(ESlateVisibility::Visible);
		InventoryBot->ItemActionMenu->EquipButton->SetVisibility(ESlateVisibility::Collapsed);
		InventoryBot->ItemActionMenu->DropButton->SetVisibility(ESlateVisibility::Visible);
		InventoryBot->ItemActionMenu->DropAllButton->SetVisibility(ESlateVisibility::Visible);
		InventoryBot->ItemActionMenu->SetActiveItem(this);
	}
}

void UInventoryItemDisplay::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseEnter(InGeometry, InMouseEvent);

	// FString MyString = FString::FromInt(GetInventoryIndex());
	// GEngine->AddOnScreenDebugMessage(-1,2.f, FColor::Red, MyString);

	InventoryBot->InventoryInfo(GeneratingInfo(true)); // Getting Information about stored Items
    //-------------------------------------------------------------------------------------- Setting Border Color for the Item under Mouse Cursor
	if(SlotBorder && WorkGrid)
	{
		SlotBorder->SetBrushColor(WorkGrid->SelectionColor.OverItemColor);
	}
}

void UInventoryItemDisplay::NativeOnMouseLeave(const FPointerEvent& InMouseEvent)
{
	if(SlotBorder && WorkGrid)
	{
		SlotBorder->SetBrushColor(BorderInitialColor);
	}

	InventoryBot->InventoryInfo(GeneratingInfo(false));
}

FReply UInventoryItemDisplay::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	InventoryBot->HideActionMenu();

	Owner->GetInventoryWidget()->InventoryGrids.Empty();
	
	FEventReply Reply;
	FEventReply Reply_L;
	FEventReply Reply_R;
	Reply.NativeReply = Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
	Reply_L.NativeReply = Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
	Reply_R.NativeReply = Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
	
	Reply_L = UWidgetBlueprintLibrary::DetectDragIfPressed(InMouseEvent,this, FKey{"LeftMouseButton"});
	Reply_R = UWidgetBlueprintLibrary::DetectDragIfPressed(InMouseEvent,this, FKey{"RightMouseButton"});
    //-------------------------------------------------------------------------------------- Preparing Coordinates for ActionMenu
	APlayerController* PC = GetWorld()->GetFirstPlayerController();
	float MousePosX = 0;
	float MousePosY = 0;
	PC->GetMousePosition(MousePosX, MousePosY);
    //-------------------------------------------------------------------------------------- Left Mouse Button Press	
	if(Reply_L.NativeReply.IsEventHandled())
	{
		Reply.NativeReply = Reply_L.NativeReply;
	}
    //-------------------------------------------------------------------------------------- Right Mouse Button Press
	if(Reply_R.NativeReply.IsEventHandled())
	{
		//-------------------------------------------------------------------------------------- Creation of the Menu (once)
		if(InventoryBot->ItemActionMenu == nullptr)
		{
			InventoryBot->ItemActionMenu = CreateWidget<UItemActionMenu>(PC, InventoryBot->ActionUIClass);
			InventoryBot->ItemActionMenu->AddToViewport(1);
			InventoryBot->ItemActionMenu->SetPositionInViewport({MousePosX, MousePosY});
			InventoryBot->ItemActionMenu->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
			InventoryBot->ItemActionMenu->SetInventoryWidget(Owner->GetInventoryWidget());
			InventoryBot->ItemActionMenu->SetActiveGrid(Owner);
			SetPossibleActions();
			FString MyString = InventoryBot->ItemActionMenu->GetName();
			GEngine->AddOnScreenDebugMessage(-1,1.f, FColor::Red, MyString);

		}
		//-------------------------------------------------------------------------------------- If Menu is already created
		else
		{
			if(InventoryBot->ItemActionMenu->GetVisibility() == ESlateVisibility::SelfHitTestInvisible)
			{
				InventoryBot->ItemActionMenu->SetVisibility(ESlateVisibility::Hidden);
			}
			else
			{
				InventoryBot->ItemActionMenu->SetPositionInViewport({MousePosX, MousePosY});
				InventoryBot->ItemActionMenu->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
				InventoryBot->ItemActionMenu->SetInventoryWidget(Owner->GetInventoryWidget());
				InventoryBot->ItemActionMenu->SetActiveGrid(Owner);
				SetPossibleActions();
			}
		}
		Reply.NativeReply = Reply_R.NativeReply;
	}
	return Reply.NativeReply;
}

FReply UInventoryItemDisplay::NativeOnMouseButtonUp(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	return FReply::Handled();
}

void UInventoryItemDisplay::NativeOnDragDetected(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent, UDragDropOperation*& OutOperation)
{
	Super::NativeOnDragDetected(InGeometry, InMouseEvent, OutOperation);
	
	Owner->GetInventoryWidget()->InventoryGrids.Add(Owner, false);
    //-------------------------------------------------------------------------------------- Check if the Drag Function should be activated
	UDragDropWidget* WidgetDrag = Cast<UDragDropWidget>(UWidgetBlueprintLibrary::CreateDragDropOperation(UDragDropWidget::StaticClass()));

	if (WidgetDrag == nullptr) {return;}

	for(auto& Key : InMouseEvent.GetPressedButtons())
	{
		if(Key == FKey{"RightMouseButton"}) {return;}
	}
    //-------------------------------------------------------------------------------------- Clone Widget for Visualization of the dragged Item
	APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	UInventoryItemDisplay* DragVisual = CreateWidget<UInventoryItemDisplay>(PC, GetClass());
	
	DragVisual->Init(ItemData);
	DragVisual->SetItemCount(this->GetItemCount());
	WidgetDrag->WidgetToDrag = this;
	WidgetDrag->Payload = this;
	WidgetDrag->DefaultDragVisual = DragVisual;
	WidgetDrag->Pivot = EDragPivot::TopLeft;
	OutOperation = WidgetDrag;
	//-------------------------------------------------------------------------------------- Map Item as 'Swapped' > hide it in the Slot > make Slot available for Items
	Owner->GetInventoryWidget()->SwappedItems.Add(this, SlotIndexOnInventory);
	
	this->SetVisibility(ESlateVisibility::Hidden);

	if(Owner)
	{
		Owner->ClearSlots(Owner->GetCoordinateByIndex(SlotIndexOnInventory));
	}
}

void UInventoryItemDisplay::NativeOnDragCancelled(const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation)
{
	Super::NativeOnDragCancelled(InDragDropEvent, InOperation);

	Owner->GetInventoryWidget()->SwappedItems.Empty();
	this->SetVisibility(ESlateVisibility::Visible);

	if(SlotBorder)
	{
		SlotBorder->SetBrushColor(BorderInitialColor);
	}
	
	if(Owner)
	{
		Owner->FillSlots(Owner->GetCoordinateByIndex(SlotIndexOnInventory));
	}
}

bool UInventoryItemDisplay::NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation)
{
	Super::NativeOnDrop(InGeometry, InDragDropEvent, InOperation);
    //-------------------------------------------------------------------------------------- Swap Items
	UDragDropWidget* DropWidget = Cast<UDragDropWidget>(InOperation);

	if (DropWidget == nullptr) {return false;}

	if(UUniformGridPanel* GridPanel = Cast<UUniformGridPanel>(GetParent()))
	{
		if(UInventoryItemDisplay* InventoryItemDisplay = Cast<UInventoryItemDisplay>(DropWidget->WidgetToDrag))
		{
			if(Owner)
			{
				Owner->GetInventoryWidget()->SwappedItems.Add(this, SlotIndexOnInventory);
				if(!Owner->IsModifierGrid && !InventoryItemDisplay->Owner->IsModifierGrid)
				{
					Owner->SwapItems();
					Owner->GetInventoryWidget()->SwappedItems.Empty();
				
					return true;
				}
				Owner->GetInventoryWidget()->SwappedItems.Empty();
			}
		}
	}
	return false;
}

void UInventoryItemDisplay::Init(UPickupDABase* BasicItemData)
{
	ItemData = DuplicateObject<UPickupDABase>(BasicItemData,this);;
	
	if(ItemData)
	{
		ItemIcon->SetBrushFromTexture(ItemData->GetThumbnail());
		//-------------------------------------------------------------------------------------- Set Size of the Item Icon
		UCanvasPanelSlot* SlotCanvas = Cast<UCanvasPanelSlot>(SlotBorder->Slot);
		const FVector2D ActualSize = SlotCanvas->GetSize();
		const FVector2D IconSize = ActualSize - 50.f;
		ItemIcon->Brush.SetImageSize(IconSize);
 
		SetStackSizeTextBlock(ItemCount); // Set visual Item Counter
	}
}

void UInventoryItemDisplay::ChangeItemCount(const int Value)
{
	ItemCount += Value;
	SetStackSizeTextBlock(ItemCount);
	if(ItemCount > 1) {SetCounterVisibility(true);}
	else SetCounterVisibility(false);
}

void UInventoryItemDisplay::SetItemCount(const int Value)
{
	ItemCount = Value;
	SetStackSizeTextBlock(ItemCount);
	if(ItemCount > 1) {SetCounterVisibility(true);}
	else SetCounterVisibility(false);
}

void UInventoryItemDisplay::SetStackSizeTextBlock(const int Stack)
{
	StackSizeTextBlock->SetText(FText::FromString(FString::Printf(TEXT("x%d"), Stack)));
}

void UInventoryItemDisplay::SetCounterVisibility(const bool VisibleCounter)
{
	if(VisibleCounter) {StackSizeTextBlock->GetParent()->SetVisibility(ESlateVisibility::Visible);}
	else StackSizeTextBlock->GetParent()->SetVisibility(ESlateVisibility::Hidden);
}
