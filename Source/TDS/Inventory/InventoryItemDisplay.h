﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "DynamicInventoryGrid.h"
#include "Blueprint/UserWidget.h"
#include "InventoryBot.h"
#include "TDS/Pickup/PickupDABase.h"

#include "InventoryItemDisplay.generated.h"

class UPickupDABase;
class UImage;
class UBorder;
class UDynamicInventoryGrid;
class UTextBlock;
/**
 * 
 */

UCLASS()
class TDS_API UInventoryItemDisplay : public UUserWidget
{
	GENERATED_BODY()

public:
	
	UPROPERTY(BlueprintReadOnly)
		FPickupDataStruct PickupData;
	UPROPERTY()
		UDynamicInventoryGrid* Owner = nullptr;
	UPROPERTY(BlueprintReadOnly)
		UPickupDABase* ItemData = nullptr;

	UFUNCTION(BlueprintCallable)
		int GetInventoryIndex() const {return SlotIndexOnInventory;}

	UFUNCTION(BlueprintCallable)
		int GetItemCount() const {return ItemCount;}
	UFUNCTION(BlueprintCallable)
		void SetItemCount (const int Value);
	
	void Init(UPickupDABase* BasicItemData);
	void ChangeItemCount(const int Value);
	void SetStackSizeTextBlock (int Stack);
	void SetCounterVisibility(bool VisibleCounter);
	void SetInventoryIndex(const int Index) {SlotIndexOnInventory = Index;}

	
private:
	
	int SlotIndexOnInventory = -1;
	int ItemCount = 1;
	
	UPROPERTY()
		UDynamicInventoryGrid* WorkGrid;

	UPROPERTY()
		AInventoryBot* InventoryBot;
	
	UPROPERTY(meta = (BindWidget))
		UImage* ItemIcon = nullptr;
	UPROPERTY(meta = (BindWidget))
		UBorder* SlotBorder = nullptr;
	UPROPERTY(meta = (BindWidget))
		UTextBlock* StackSizeTextBlock = nullptr;
 	
	FLinearColor BorderInitialColor;
	FItemInfoStruct ItemInfoStruct; 
	
	FItemInfoStruct GeneratingInfo(bool OverItem);
	
	void SetPossibleActions();
	
	virtual void NativeConstruct() override;
	
	virtual void NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	virtual void NativeOnMouseLeave(const FPointerEvent& InMouseEvent) override;

	virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	virtual FReply NativeOnMouseButtonUp(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;

	virtual void NativeOnDragDetected(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent, UDragDropOperation*& OutOperation) override;
	virtual void NativeOnDragCancelled(const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation) override;
	
	virtual bool NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation) override;
};
