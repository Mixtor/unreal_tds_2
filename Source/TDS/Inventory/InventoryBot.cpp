﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryBot.h"

#include "InventoryWidget.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Misc/OutputDeviceNull.h"
// #include "TDS/TDSCharacter.h"
#include "TDS/TDSGameMode.h"

// Sets default values
AInventoryBot::AInventoryBot()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	Tags.Add("Inventory");

	GetCapsuleComponent()->SetGenerateOverlapEvents(false);
	
	BotMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MC"));
	BotMesh->SetupAttachment(GetCapsuleComponent());
	//-------------------------------------------------------------------------------------- Scanner for registry Pickups around the Bot in certain Radius
	Pickup_Scanner = CreateDefaultSubobject<USphereComponent>(TEXT("PS"));
	Pickup_Scanner->SetupAttachment(GetCapsuleComponent());
	Pickup_Scanner->SetGenerateOverlapEvents(false);
	//-------------------------------------------------------------------------------------- Slots for Modifications should be placed here
	Slots = CreateDefaultSubobject<UArrowComponent>(TEXT("AC"));
	Slots->SetupAttachment(GetCapsuleComponent());
	
	BotInventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("IC")); // Assigning preferred InventoryWidget 
	//-------------------------------------------------------------------------------------- Variable to Access ItemActionMenu
	static ConstructorHelpers::FClassFinder<UItemActionMenu> Test (TEXT("/Game/Widgets/InventorySystem/W_ItemActionMenu"));
	ActionUIClass = Test.Class;
}

FHitResult AInventoryBot::TraceCheck(FVector Start, FVector End, FCollisionObjectQueryParams Objects)
{
	FHitResult HitObject;
	GetWorld()->LineTraceSingleByObjectType(HitObject, Start, End, Objects);

	return HitObject;
}

// Called when the game starts or when spawned
void AInventoryBot::BeginPlay()
{
	Super::BeginPlay();
	
	SetActorTickEnabled(false);
	
	if(IsCollector) GetCapsuleComponent()->ComponentTags.Add("Collector"); // Set Status of the InventoryBot
	
	SetInputKeysArray(); // Creating Array for controlling Movement of the Bot
	SetSpawnPoints();	// Creating Array of SpawnPoints for Pickups
	//-------------------------------------------------------------------------------------- Set Scanner for Pickups
	MaxScanRadius = Pickup_Scanner->GetScaledSphereRadius();
	ClosestDistance = MaxScanRadius + 10.f;
	LaunchScanner();
}

// Called every frame
void AInventoryBot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ScanProcedure(DeltaTime);
	//-------------------------------------------------------------------------------------- Launching Scanner that checks if there is Ground for continuous Movement
	if(GroundCheck)
	{
		ACharacter* Player = UGameplayStatics::GetPlayerCharacter(GetWorld(),0);
	
		const FCollisionObjectQueryParams Objects = ECC_WorldStatic;
		const FVector PartOne = (GetActorLocation() - Player->GetActorLocation()).GetSafeNormal() * ScanDistance + Player->GetActorLocation();
		const FVector PartTwo = GetRootComponent()->GetUpVector()* -ScanDepth;
		const FVector Start = {PartOne.X, PartOne.Y, GetActorLocation().Z};
		const FVector End = PartOne + PartTwo;
	
		IsGround = TraceCheck(Start, End, Objects).bBlockingHit;
	}
	//-------------------------------------------------------------------------------------- Acquiring Data for AIController
	if(Available)
	{
		if(GetController())
		{
			if(!GetController()->IsPlayerController())
			{
				BotDistance = MeasureBotDistance(); // Get Distance between InventoryBot and PlayerCharacter
				if(GetController()) {BotDistanceConditions(BotDistance);} // Check Events that should occur on certain Distance between InventoryBot and Player
			}
		}
	}
}

// Called to bind functionality to input
void AInventoryBot::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AInventoryBot::SetInputKeysArray()
{
	//-------------------------------------------------------------------------------------- Get instance of the InputSettings
	UInputSettings* InputSettings = UInputSettings::GetInputSettings();
	//-------------------------------------------------------------------------------------- AxisMappings with all the information will be stored here
	TArray<FInputAxisKeyMapping> VerticalKeys;
	TArray<FInputAxisKeyMapping> HorizontalKeys;
	//-------------------------------------------------------------------------------------- Each MovementKey gets its own variable
	FKey ForwardKey;
	FKey BackKey;
	FKey LeftKey;
	FKey RightKey;
	//-------------------------------------------------------------------------------------- Load the AxisMappings
	InputSettings->GetAxisMappingByName(FName("MoveForward"), VerticalKeys);
	InputSettings->GetAxisMappingByName(FName("MoveRight"), HorizontalKeys);
	//-------------------------------------------------------------------------------------- Assign each key to the correct direction
	for (const FInputAxisKeyMapping VerticalKey : VerticalKeys)
	{
		if (VerticalKey.Scale == 1.0f)
			ForwardKey = VerticalKey.Key;
		else if (VerticalKey.Scale == -1.0f)
			BackKey = VerticalKey.Key;
	}

	for (const FInputAxisKeyMapping HorizontalKey : HorizontalKeys)
	{
		if (HorizontalKey.Scale == 1.0f)
			RightKey = HorizontalKey.Key;
		else if (HorizontalKey.Scale == -1.0f)
			LeftKey = HorizontalKey.Key;
	}
	//-------------------------------------------------------------------------------------- Assembling Array
	InputKeys.Add(ForwardKey);
	InputKeys.Add(RightKey);
	InputKeys.Add(BackKey);
	InputKeys.Add(LeftKey);
}

void AInventoryBot::SetSpawnPoints()
{
	const float SpawnPoint_F = SpawnDistance;
	const float SpawnPoint_B= -SpawnDistance;
	const float SpawnPoint_L = -SpawnDistance;
	const float SpawnPoint_R = SpawnDistance;
	
	SpawnPoints.Add(SpawnPoint_F);
	SpawnPoints.Add(SpawnPoint_B);
	SpawnPoints.Add(SpawnPoint_L);
	SpawnPoints.Add(SpawnPoint_R);
}

void AInventoryBot::StartScanTimer()
{
	if(GetWorldTimerManager().IsTimerActive(ScanTimerHandle))
	{
		ControllingOverlaps(true, true);
	}
	else GetWorldTimerManager().SetTimer(ScanTimerHandle, this, &AInventoryBot::StartScanTimer, ScanPeriod, true);

	Pickup_Scanner->SetSphereRadius(0.f);
}

void AInventoryBot::LaunchScanner() const
{
	if(IsCollector)
	{
		Pickup_Scanner->OnComponentBeginOverlap.AddDynamic(this, &AInventoryBot::OverlapCheck);
	}
	else Pickup_Scanner->OnComponentBeginOverlap.RemoveDynamic(this, &AInventoryBot::OverlapCheck);
}

void AInventoryBot::DeactivateBot()
{
	ATDSCharacter* Character = Cast<ATDSCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	
	Available = false;
	BotInventoryComponent->InventoryWidget->ClearWidgets(UInventoryWidget::StaticClass());
	GetController()->SetActorTickEnabled(false);
	SetActorTickEnabled(false);
	GetController()->UnPossess();
	SetOwningCharacter(nullptr);
	
	Character->InventoryBot = nullptr;

	StateCheck();
}

void AInventoryBot::ControllingOverlaps(bool Collector, bool Scanner) const
{
	GetCapsuleComponent()->SetGenerateOverlapEvents(Collector);
	Pickup_Scanner->SetGenerateOverlapEvents(Scanner);
}

bool AInventoryBot::InventoryCheck(AActor* Pickup) const
{
	const int InventoryVolume = BotInventoryComponent->InventoryWidget->InventoryGridPanel->GetInventoryComponent()->GetInventoryVolume();
	APickupBase* Item = Cast<APickupBase>(Pickup);
	const int ItemVolume = Item->ItemData->GetItemVolume();
	const int Availability = BotInventoryComponent->InventoryWidget->InventoryGridPanel->GetFirstAvailableSlotIndex();
	
	if(ItemVolume > InventoryVolume) {return false;}
	
	int Count = 1;
	for(auto& ItemToCheck : BotInventoryComponent->InventoryWidget->InventoryGridPanel->ItemDisplayContainer)
	{
		if(ItemToCheck->ItemData->GetId() != Item->ItemData->GetId() && Availability == -1)
		{
			if(Count == BotInventoryComponent->InventoryWidget->InventoryGridPanel->ItemDisplayContainer.Num()) {return false;}
		}
		Count ++;
		if(ItemToCheck->ItemData->GetId() == Item->ItemData->GetId() && !Item->ItemData->CheckStackable()) {return false;}
		if(ItemToCheck->ItemData->GetId() == Item->ItemData->GetId() && Item->ItemData->CheckStackable()) {return true;}
	}
	return true;
}

void AInventoryBot::ScanProcedure(float DeltaTime)
{
	const float CurrentScanRadius = FMath::FInterpTo(Pickup_Scanner->GetScaledSphereRadius(), MaxScanRadius, DeltaTime, 4.f);
	Pickup_Scanner->SetSphereRadius(CurrentScanRadius);
}

void AInventoryBot::OverlapCheck(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                 const FHitResult& SweepResult)
{
	//-------------------------------------------------------------------------------------- Preparing Data for AI Controller
	FOutputDeviceNull Ar;
	const FVector TargetVector = OtherActor->GetActorLocation();
	const FString PickupPosition = FString::Printf(TEXT("OverlappingPickup %s"), *TargetVector.ToString()); // In BP should be available Event with a Name equal to TEXT
	//-------------------------------------------------------------------------------------- Is it possible to pick the Item up 
	APickupBase* TargetPickup = Cast<APickupBase>(OtherActor);

	if(InventoryCheck(TargetPickup) && TargetPickup->IsCollectable())
	{
		ControllingOverlaps(true, false);
		
		if(GetController())
		{
			GetController()->CallFunctionByNameWithArguments(*PickupPosition, Ar, nullptr, true);
		}
	}
}

float AInventoryBot::MeasureBotDistance() const
{
	TArray<AActor*> OutActors;
	UGameplayStatics::GetAllActorsWithTag(this, "Player", OutActors);
	return GetHorizontalDistanceTo(OutActors[0]);
}

void AInventoryBot::BotDistanceConditions(const float Distance)
{
	if(IsCalling && !Return)
	{
		if(Distance <= 160.f)
		{
			ToggleInventory();
		}
	}
	if(Distance < 300.f && Return)
	{
		Return = false;
		EventCallBot(false);
	}
	if(Distance > ReturnDistance)
	{
		Return = true;
		EventCallBot(true);
	}
	if(Distance > DeactivationDistance)
	{
		DeactivateBot();
	}
}

void AInventoryBot::CheckAvailablePickups() const
{
	TArray<AActor*> FoundActors;
	Pickup_Scanner->GetOverlappingActors(FoundActors, APickupBase::StaticClass());
	
	for(auto& Pickup : FoundActors)
	{
		APickupBase* Item = Cast<APickupBase>(Pickup);
		Item->SetCollectable(true);
	}
}

void AInventoryBot::HideActionMenu() const
{
	if(ItemActionMenu)
	{
		ItemActionMenu->SetVisibility(ESlateVisibility::Hidden);
	}
}

bool AInventoryBot::ToggleInventory()
{
	IsCalling = false;

	{ 
		if(UInventoryWidget* InventoryWidget = BotInventoryComponent->InventoryWidget)
		{	
			ATDSCharacter* Player = Cast<ATDSCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());

			if(IsInventoryActive)
			{	
				InventoryWidget->SetVisibility(ESlateVisibility::Hidden);
				IsInventoryActive = false;
				if(Player) {Player->Stand = false;}
				BotInventoryComponent->InventoryWidget->ClearWidgets(UInventoryItemDisplay::StaticClass());
				BotInventoryComponent->InventoryWidget->WidgetActivation(false);

				LaunchScanner();
				EventCallBot(false);
			}
			else
			{
				InventoryWidget->SetVisibility(ESlateVisibility::Visible);
				IsInventoryActive = true;
				if(Player) {Player->Stand = true;}
				BotInventoryComponent->InventoryWidget->WidgetActivation(true);

				Pickup_Scanner->OnComponentBeginOverlap.RemoveDynamic(this, &AInventoryBot::OverlapCheck);
			}
			EnableUIMode(IsInventoryActive);
		}
	}
	return IsInventoryActive;
}

void AInventoryBot::StateCheck_Implementation()
{

}

void AInventoryBot::EnableUIMode(const bool IsEnable) const
{
	if (APlayerController* PC = GEngine->GetFirstLocalPlayerController(GWorld))
	{
		if(IsEnable)
		{
			UWidgetBlueprintLibrary::SetInputMode_GameAndUIEx(PC, BotInventoryComponent->InventoryWidget);
			PC->bShowMouseCursor = true;
		}
		else
		{
			UWidgetBlueprintLibrary::SetInputMode_GameOnly(PC);
			PC->bShowMouseCursor = false;
		}
	}
}

void AInventoryBot::EventCallBot(const bool Calling)
{
	FOutputDeviceNull Ar;
	SetCallStatus(Calling);
	const bool bCall = Calling;
	const FString Call = FString::Printf(TEXT("CallBot %d"), bCall); // In BP should be available Event with a Name equal to TEXT
	GetController()->CallFunctionByNameWithArguments(*Call, Ar, nullptr, true);
}

void AInventoryBot::InventoryInfo(const FItemInfoStruct ItemInfoStruct) const
{
	UItemInfo* NewInfo = BotInventoryComponent->InventoryWidget->ItemInfo;
	
	FString VolumeString = FString::FromInt(ItemInfoStruct.ItemVolume);
	const FString OverallVolume = FString::FromInt(ItemInfoStruct.ItemVolume * ItemInfoStruct.OverallItems);
	VolumeString = "Volume: " + VolumeString + " (" + OverallVolume + ")";
	
	NewInfo->Name->SetText(ItemInfoStruct.ItemName);
	NewInfo->Description-> SetText(FText::FromString(ItemInfoStruct.Description));
	NewInfo->Volume->SetText(FText::FromString(VolumeString));
}
