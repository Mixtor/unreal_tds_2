﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "InventoryEnums.generated.h"

UENUM()
enum EItemType
{
	EIT_Item UMETA(DisplayName = "Item"),
	EIT_Enhancer UMETA(DisplayName = "Enhancer"),
	EIT_Modifier UMETA(DisplayName = "Modifier"),

	EIT_Null UMETA(Hidden)
};

