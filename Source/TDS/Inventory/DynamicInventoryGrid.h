﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InventoryComponent.h"
#include "Blueprint/UserWidget.h"
#include "Components/UniformGridPanel.h"

#include "DynamicInventoryGrid.generated.h"

class UBorder;
class UPickupDABase;
class UInventoryItemDisplay;
class UInventorySlot;
/**
 * 
 */
USTRUCT()
struct FColorStruct
{
	GENERATED_BODY()
	
	UPROPERTY()
		FLinearColor BorderInitialColor = {1, 1, 1, 0};
	UPROPERTY()
		FLinearColor ActivatedSlotColor = {1, 0, 0, 1};
	UPROPERTY()
		FLinearColor SelectedSlotColor = {0, 0.5, 1, 1};
	UPROPERTY()
		FLinearColor OverItemColor = {1, 1, 0, 1};
	UPROPERTY()
		FLinearColor OverSlotColor = {0, 1, 0, 1};
};

UCLASS()
class TDS_API UDynamicInventoryGrid : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadWrite, meta=(BindWidget))
		UUniformGridPanel* InventoryGrid = nullptr;

	UPROPERTY(EditInstanceOnly, Category="Settings",  meta = (ClampMin = "1", ClampMax = "50", UIMin = "1", UIMax = "50", ExpseOnSpawn = true))
		int ColumnCount = 4;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Settings",  meta = (ClampMin = "1", ClampMax = "50", UIMin = "1", UIMax = "50", ExpseOnSpawn = true))
		int RowCount = 1;
	UPROPERTY(EditInstanceOnly, Category="Settings",  meta = (ClampMin = "50", ClampMax = "250", UIMin = "50", UIMax = "250", ExpseOnSpawn = true))
		float TileSize = 250.f;
	UPROPERTY(EditAnywhere, Category="Settings")
		bool IsModifierGrid = false;
	UPROPERTY(EditAnywhere, Category="Settings")
		FString SlotTypeArray = "";
	
	UPROPERTY()
		UInventorySlot* SelectedSlot = nullptr;

	UPROPERTY()
		TArray<UInventorySlot*> Slots;
	UPROPERTY()
		TMap<UInventorySlot*, bool> SlotMap;
	UPROPERTY(BlueprintReadWrite)
		TArray<UInventoryItemDisplay*> ItemDisplayContainer;

	UPROPERTY()
		FPickupDataStruct PickupData;
	UPROPERTY()
		FColorStruct SelectionColor;
	
	UFUNCTION(BlueprintCallable)
		UInventoryComponent* GetInventoryComponent() {return InventoryComponent;}
	UFUNCTION(BlueprintCallable)
		UInventoryWidget* GetInventoryWidget() {return InventoryWidget;}

	UFUNCTION()
		bool AddItem(UPickupDABase* Item_Data, FPickupDataStruct Pickup_Data, int PosIndex);
	UFUNCTION()
		void RemoveItem(UPickupDABase* ItemDataAsset);
	UFUNCTION(BlueprintCallable)
		bool IsItemAvailableForSlot(const int Index) const;
	UFUNCTION(BlueprintCallable)
		int GetSlotIndexByCoordinate(const int Column, const int Row) const;
	UFUNCTION(BlueprintCallable)
		FIntPoint GetCoordinateByIndex(const int Index) const;
	UFUNCTION()
		void SwapItems();

	UFUNCTION(BlueprintCallable)
		virtual void SynchronizeProperties() override;
	
	void FillSlots(const FIntPoint& PlacePoint);
	void ClearSlots(const FIntPoint& PlacePoint);
	void SetInventoryComponent(UInventoryComponent* InvComp);
	void ReconstructInventoryGrid();
	void ReconstructModifierGrid();

	int GetFirstAvailableSlotIndex() const;

	bool EquipItem(UDynamicInventoryGrid* Grid, UInventoryItemDisplay* Item);
	
	UFUNCTION(BlueprintCallable)
		UPARAM (DisplayName="Item") UInventoryItemDisplay* SetInventoryData(UPickupDABase* Item_Data, FPickupDataStruct Pickup_Data, int Index);
	
protected:
	
	UPROPERTY(meta = (BindWidget))
		UBorder* GridPanelBorder = nullptr;
	UPROPERTY(EditDefaultsOnly, Category = "Class Settings")
		TSubclassOf<UInventoryItemDisplay> ItemDisplayClass;
	UPROPERTY(EditDefaultsOnly, Category = "Class Settings")
		TSubclassOf<UInventorySlot> InventorySlotClass;
	
private:

	UPROPERTY()
		UInventoryComponent* InventoryComponent = nullptr;
	UPROPERTY()
		UInventoryWidget* InventoryWidget = nullptr;
	UPROPERTY()
		TArray<bool> SlotType;
	
	void BuildSlotTypeArray();
	void SetSlotType();
	
	virtual void NativeConstruct() override;
};
