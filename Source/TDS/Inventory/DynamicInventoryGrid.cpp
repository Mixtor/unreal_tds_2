﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "DynamicInventoryGrid.h"

#include "Blueprint/WidgetTree.h"
#include "Components/Border.h"
#include "Components/Image.h"
#include "InventoryWidget.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Components/UniformGridSlot.h"
#include "InventorySlot.h"

void UDynamicInventoryGrid::NativeConstruct()
{
	Super::NativeConstruct();

	TArray<UUserWidget*> Inventory;
	UWidgetBlueprintLibrary::GetAllWidgetsOfClass(this, Inventory, UInventoryWidget::StaticClass(), false);
	InventoryWidget = Cast<UInventoryWidget>(Inventory[0]);
}

bool UDynamicInventoryGrid::AddItem(UPickupDABase* Item_Data, FPickupDataStruct Pickup_Data, int PosIndex)
{
	int Index = 0;
	//-------------------------------------------------------------------------------------- Check for free Slots and stackable Items in the Inventory
	if(PosIndex == -1)
	{
		Index = GetFirstAvailableSlotIndex();

		for(const auto ItemRef : ItemDisplayContainer)
		{
			if(ItemRef->ItemData->GetId() == Item_Data->GetId())
			{
				if(Item_Data->CheckStackable())
				{
					ItemRef->ChangeItemCount(1);
					if(ItemRef->GetItemCount() > 1) {ItemRef->SetCounterVisibility(true);}
					
					return true;
				}
				return false;
			}
		}
	}
	else Index = PosIndex;
	
	if (Index == -1) {return false;}
	
	SetInventoryData(Item_Data, Pickup_Data, Index); // Add new Item to the Inventory
	
	return true;
}

void UDynamicInventoryGrid::RemoveItem(UPickupDABase* ItemDataAsset)
{
	UInventoryItemDisplay* DeleteItem = nullptr;

	for	(const auto ItemDisplay : ItemDisplayContainer)
	{
		if(ItemDataAsset->GetId() == ItemDisplay->ItemData->GetId())
		{
			DeleteItem = ItemDisplay;
		}
	}
	if(DeleteItem)
	{
		ItemDisplayContainer.Remove(DeleteItem);
	}
}

void UDynamicInventoryGrid::SynchronizeProperties()
{
	Super::SynchronizeProperties();

	ReconstructInventoryGrid();
	if(IsModifierGrid) ReconstructModifierGrid();
}

void UDynamicInventoryGrid::ReconstructInventoryGrid() 
{
	//-------------------------------------------------------------------------------------- Reconstruct Inventory Slots
	if (InventoryGrid && InventorySlotClass && !IsModifierGrid)
	{
		InventoryGrid->ClearChildren();
		Slots.Empty();
		SlotMap.Empty();
		
		if(InventoryComponent) {InventoryComponent->InventoryWidget->SetStack(true);}
		int Count = 0;
		for (int32 y = 0; y < RowCount; ++y)
		{
			for (int32 x = 0; x < ColumnCount; ++x)
			{
				UInventorySlot* InventorySlot = WidgetTree->ConstructWidget<UInventorySlot>(InventorySlotClass);
				InventorySlot->SetCoordinate(y, x);
				InventorySlot->Owner = this;
				InventorySlot->SetIndex(Count);

				if(InventoryComponent)
				{
					for(auto& SingleSlot : InventoryComponent->InventoryWidget->DropSlots)
					{
						if(Count == SingleSlot.Key->GetIndex()) {InventorySlot = SingleSlot.Key;}
					}
					InventoryComponent->InventoryWidget->ForwardButton->GetParent()->SetVisibility(ESlateVisibility::Hidden);
				}

				if(Count > 3 && InventoryComponent->InventoryWidget->AdditionalSlots.Num() != 4)
				{
					FButtonStruct New;
					New.ActiveButtons[Count - 4] = true;
					InventoryComponent->InventoryWidget->AdditionalSlots.Add(InventorySlot, New);
				}
				else if(InventoryComponent) InventoryComponent->InventoryWidget->ReadyPickup(true, true, nullptr);

				Count++;

				InventoryGrid->AddChildToUniformGrid(InventorySlot, y, x);
				Slots.Add(InventorySlot);
				SlotMap.Add(InventorySlot, false);
			}
		}
		if(InventoryComponent) {InventoryComponent->InventoryWidget->SetStack(false);}
		//-------------------------------------------------------------------------------------- Reconstruct Inventory DropSlots
		if(InventoryComponent)
		{
			for(auto& SingleSlot : InventoryComponent->InventoryWidget->DropSlots)
			{
				if(SingleSlot.Key->GetIndex() < SlotMap.Num())
				{
					int SingleSlotIndex = SingleSlot.Key->GetIndex();
					for(auto& AvailableSlot : SlotMap)
					{
						if(AvailableSlot.Key->GetIndex() == SingleSlotIndex)
						{
							AvailableSlot.Key->SlotBorder->SetBrushColor(SelectionColor.ActivatedSlotColor);
						}
					}
				}
				else
				{
					for(int i = 0; i < 4; i++)
					{
						if(SingleSlot.Value.ActiveButtons[i])
						{
							InventoryComponent->InventoryWidget->WidgetButtons[i]->SetIsEnabled(true);
							
							InventoryComponent->InventoryWidget->SlotNumbers[i]->SetVisibility(ESlateVisibility::Hidden);
						}
					}
					InventoryComponent->InventoryWidget->DropSlots.Remove(SingleSlot.Key);
				}
			}
		}
		//-------------------------------------------------------------------------------------- Reconstruct Inventory Items
		if(ItemDisplayContainer.Num() != 0)
		{
			for(auto& Item : ItemDisplayContainer)
			{
				const int ImageIndex = Item->GetInventoryIndex();
				const FIntPoint ImageCoordinate = GetCoordinateByIndex(ImageIndex);
				//-------------------------------------------------------------------------------------- Item is deleted from Inventory or stay in it
				if(ImageIndex > ColumnCount * RowCount - 1)
				{
					const int ItemCount = Item->GetItemCount();
					const int ItemValue = Item->ItemData->GetItemVolume();
					const int OverallValue = ItemValue * ItemCount;
					InventoryComponent->ChangeInventoryVolume(-OverallValue);
					Item = nullptr;
				}
				else
				{
					InventoryGrid->AddChildToUniformGrid(Item, ImageCoordinate.Y, ImageCoordinate.X);
					FillSlots(ImageCoordinate);
				}
			}
			ItemDisplayContainer.RemoveAll([](const UInventoryItemDisplay* Ptr){return Ptr == nullptr;});
			InventoryComponent->InventoryWidget->VolumeBarState();
		}
	}
}

void UDynamicInventoryGrid::ReconstructModifierGrid()
{
	int Count = 0;
	for (int32 y = 0; y < RowCount; ++y)
	{
		for (int32 x = 0; x < ColumnCount; ++x)
		{
			UInventorySlot* InventorySlot = WidgetTree->ConstructWidget<UInventorySlot>(InventorySlotClass);
			InventorySlot->SetCoordinate(y, x);
			InventorySlot->SetIndex(Count);
			InventorySlot->Owner = this;
	
			Count++;

			InventoryGrid->AddChildToUniformGrid(InventorySlot, y, x);
			Slots.Add(InventorySlot);
			SlotMap.Add(InventorySlot, false);
		}
	}
	BuildSlotTypeArray();
	SetSlotType();
}

int UDynamicInventoryGrid::GetFirstAvailableSlotIndex() const
{
	int i = 0;
	for (const auto& SingleSlot : SlotMap)
	{
		if(IsItemAvailableForSlot(i)) {return i;}
		i++;
	}
	return -1;
}

bool UDynamicInventoryGrid::EquipItem(UDynamicInventoryGrid* Grid, UInventoryItemDisplay* Item)
{
	UDynamicInventoryGrid* Modifier = InventoryWidget->ModifierGridPanel;
	UDynamicInventoryGrid* Inventory = InventoryWidget->InventoryGridPanel;
	//-------------------------------------------------------------------------------------- Moving Item from Inventory to Modifier Grid
	if(Grid != Modifier)
	{
		if(Item->ItemData->GetPickupType() == EIT_Enhancer)
		{
			//-------------------------------------------------------------------------------------- Check if the Enhancer is not already installed
			for(auto& EquippedItem: Modifier->ItemDisplayContainer)
			{
				if(EquippedItem->ItemData->GetId() == Item->ItemData->GetId()) {return false;}
			}
			//-------------------------------------------------------------------------------------- Check and Equip Item if there is an empty Slot for the Enhancer
			for(auto& SingleSlot : Modifier->SlotMap)
			{
				if(SingleSlot.Key->SlotType == EIT_Enhancer && SingleSlot.Value == false)
				{
					Modifier->SetInventoryData(Item->ItemData, Item->PickupData, SingleSlot.Key->GetIndex());

					TMap<UPickupDABase*, bool> NewItem;
					NewItem.Add(Item->ItemData, true);
					InventoryWidget->Enhancement(NewItem);
					
					InventoryWidget->InventoryGridPanel->RemoveItem(Item->ItemData);
					Item->SetVisibility(ESlateVisibility::Hidden);
					Inventory->ClearSlots(Inventory->GetCoordinateByIndex(Item->GetInventoryIndex()));
					
					return true;
				}
			}
			return false;
		}
		//-------------------------------------------------------------------------------------- Check if the Modifier is not already installed
		for(auto& EquippedItem: Modifier->ItemDisplayContainer)
		{
			if(EquippedItem->ItemData->GetId() == Item->ItemData->GetId()) {return false;}
		}
		//-------------------------------------------------------------------------------------- Check and Equip Item if there is an empty Slot for the Modifier
		for(auto& SingleSlot : Modifier->SlotMap)
		{
			if(SingleSlot.Key->SlotType == EIT_Modifier && SingleSlot.Value == false)
			{	
				Modifier->SetInventoryData(Item->ItemData, Item->PickupData, SingleSlot.Key->GetIndex());

				TMap<UPickupDABase*, bool> NewItem;
				NewItem.Add(Item->ItemData, true);
				InventoryWidget->Modification(NewItem);
					
				InventoryWidget->InventoryGridPanel->RemoveItem(Item->ItemData);
				Item->SetVisibility(ESlateVisibility::Hidden);
				Inventory->ClearSlots(Inventory->GetCoordinateByIndex(Item->GetInventoryIndex()));
				
				return true;
			}
		}
		return false;
	}
	//-------------------------------------------------------------------------------------- Moving Item from Modifier to Inventory Grid
	for(auto& SingleSlot : Inventory->SlotMap)
	{
		if(SingleSlot.Value == false)
		{
			Inventory->SetInventoryData(Item->ItemData, Item->PickupData, SingleSlot.Key->GetIndex());

			TMap<UPickupDABase*, bool> NewItem;
			NewItem.Add(Item->ItemData, false);
			
			for(auto& Pickup : NewItem)
			{
				if(Pickup.Key->GetPickupType() == EIT_Enhancer)
				{
					InventoryWidget->Enhancement(NewItem);
				}
				else InventoryWidget->Modification(NewItem);
			}
		
			InventoryWidget->ModifierGridPanel->RemoveItem(Item->ItemData);
			Item->SetVisibility(ESlateVisibility::Hidden);
				
			return true;
		}
	}
	return false;
}

UInventoryItemDisplay* UDynamicInventoryGrid::SetInventoryData(UPickupDABase* Item_Data, FPickupDataStruct Pickup_Data, int Index)
{
	UInventoryItemDisplay* InventoryItemDisplay = CreateWidget<UInventoryItemDisplay>(this, ItemDisplayClass);
	InventoryItemDisplay->Owner = this;
	InventoryItemDisplay->Init(Item_Data);
	InventoryItemDisplay->SetInventoryIndex(Index);
	InventoryItemDisplay->PickupData = Pickup_Data;
	if(InventoryItemDisplay->GetItemCount() == 1) {InventoryItemDisplay->SetCounterVisibility(false);}
	const FIntPoint ItemPlacePoint = GetCoordinateByIndex(Index);
		
	InventoryGrid->AddChildToUniformGrid(InventoryItemDisplay, ItemPlacePoint.Y, ItemPlacePoint.X);
	ItemDisplayContainer.Add(InventoryItemDisplay);
	InventoryWidget->VolumeBarState();
	FillSlots(ItemPlacePoint);

	return InventoryItemDisplay;
}

void UDynamicInventoryGrid::BuildSlotTypeArray()
{
	for(auto& SingleSlot : Slots)
	{
		SlotType.Add(false);
	}

	if(!SlotTypeArray.IsEmpty())
	{
		int Count = 0;
		for(auto& SingleChar : SlotTypeArray)
		{
			if(SingleChar == '1' && Count < SlotType.Num())
			{
				SlotType[Count] = true;
			}
			Count++;
		}
	}
}

void UDynamicInventoryGrid::SetSlotType()
{
	for(int i = 0; i < Slots.Num(); i++)
	{
		if(SlotType[i] == false)
		{
			Slots[i]->SlotType = EIT_Enhancer;
			Slots[i]->SlotImage->SetBrush(Slots[i]->EnhancerSlot)/*WidgetStyle.SetNormal(Slots[i]->EnhancerSlot)*/;
		}
		else
		{
			Slots[i]->SlotType = EIT_Modifier;
			Slots[i]->SlotImage->SetBrush(Slots[i]->ModifierSlot)/*WidgetStyle.SetNormal(Slots[i]->ModifierSlot)*/;
		}
	}
}

FIntPoint UDynamicInventoryGrid::GetCoordinateByIndex(const int Index) const
{
	if(Index >= 0 && Index < Slots.Num())
	{
		if(UInventorySlot* InvSlot = Cast<UInventorySlot>(Slots[Index]))
		{
			if(UUniformGridSlot* GridSlot = Cast<UUniformGridSlot>(InvSlot->Slot))
			{
				return FIntPoint{GridSlot->Column, GridSlot->Row};
			}	
		}
	}
	return FIntPoint{-1,-1};
}

void UDynamicInventoryGrid::SwapItems()
{
	//-------------------------------------------------------------------------------------- Assign new Item Position & Deleting the Images
	int Index = 0;
	TArray<int> Positions = {0, 0};
	for(auto& Item : InventoryWidget->SwappedItems)
	{
		Positions[Index] = Item.Value;
		Item.Key->RemoveFromParent();
		Index++;
	}
	//-------------------------------------------------------------------------------------- Clear ItemDisplayContainer
	for(auto& Item : InventoryWidget->SwappedItems)
	{
		RemoveItem(Item.Key->ItemData);
	}
	//-------------------------------------------------------------------------------------- Set Images at new Positions
	int NewIndex = 1;
	
	for(auto& Item : InventoryWidget->SwappedItems)
	{
		if(NewIndex > -1)
		{
			AddItem(Item.Key->ItemData, Item.Key->PickupData, Positions[NewIndex]);
			NewIndex--;
			
		}
	}
	
	for(auto& NewItem : InventoryWidget->InventoryGridPanel->ItemDisplayContainer)
	{
		for(auto& OldItem : InventoryWidget->SwappedItems)
		{
			if(OldItem.Key->ItemData->GetItemName().ToString() == NewItem->ItemData->GetItemName().ToString())
			{
				NewItem->SetItemCount(OldItem.Key->GetItemCount());
			}
		}
	}
}

void UDynamicInventoryGrid::FillSlots(const FIntPoint& PlacePoint)
{
	auto& SingleSlot = Slots[GetSlotIndexByCoordinate(PlacePoint.X,PlacePoint.Y)];
	SlotMap.Add(SingleSlot, true);
}

void UDynamicInventoryGrid::ClearSlots(const FIntPoint& PlacePoint)
{
	auto& SingleSlot = Slots[GetSlotIndexByCoordinate(PlacePoint.X, PlacePoint.Y)];
	SlotMap.Add(SingleSlot, false);
}

void UDynamicInventoryGrid::SetInventoryComponent(UInventoryComponent* InvComp)
{
	InventoryComponent = InvComp;
}

int UDynamicInventoryGrid::GetSlotIndexByCoordinate(const int Column, const int Row) const
{
	for(auto& SingleSlot : Slots)
	{
		if(SingleSlot->GetSlotCoordinates() == FIntPoint{Column, Row})
		{
			return SingleSlot->GetIndex();
		}
	}
	return -1;
}

bool UDynamicInventoryGrid::IsItemAvailableForSlot(const int Index) const
{
	if(SlotMap[Slots[Index]])
	{
		return false;
	}
	return true;
}
