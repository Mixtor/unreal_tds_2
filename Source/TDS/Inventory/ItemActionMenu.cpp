﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemActionMenu.h"

#include "TDS/TDSCharacter.h"
#include "InventoryWidget.h"

void UItemActionMenu::NativeConstruct()
{
	Super::NativeConstruct();

	UseButton->OnClicked.AddDynamic(this, &UItemActionMenu::OnClickUseButton);
	EquipButton->OnClicked.AddDynamic(this, &UItemActionMenu::OnClickEquipButton);
	DropButton->OnClicked.AddDynamic(this, &UItemActionMenu::OnClickDropButton);
	DropAllButton->OnClicked.AddDynamic(this, &UItemActionMenu::OnClickDropAllButton);
}

void UItemActionMenu::OnClickUseButton()
{
	ATDSCharacter* Character = Cast<ATDSCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());
	UDynamicInventoryGrid* Grid = ActiveItem->Owner;
	UInventoryComponent* InventoryComponent = Grid->GetInventoryComponent();

	if(Character)
	{
		if(Character->CharacterHealthComponent->GetCurrentHealth() < Character->CharacterHealthComponent->GetMaxHealth())
		{
			Character->CharacterHealthComponent->ChangeHealthValue(ActiveItem->ItemData->GetItemValue());

			ActiveItem->ChangeItemCount(-1);
		
			if(ActiveItem->GetItemCount() == 0)
			{
				Grid->RemoveItem(ActiveItem->ItemData);
				Grid->ReconstructInventoryGrid();
			}

			InventoryComponent->ChangeInventoryVolume(-ActiveItem->ItemData->GetItemVolume());
			InventoryWidget->VolumeBarState();
		}
	}
	
	SetVisibility(ESlateVisibility::Hidden);
}

void UItemActionMenu::OnClickEquipButton()
{
	if(ActiveGrid == InventoryWidget->InventoryGridPanel)
	{
		if(InventoryWidget->ModifierGridPanel->EquipItem(ActiveGrid, ActiveItem))
		{
			ActiveGrid->SlotMap.Add(ActiveGrid->Slots[ActiveItem->GetInventoryIndex()], false);
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1,2.f, FColor::Yellow, "Not possible!");
		}
	}
	else
	{
		if(InventoryWidget->InventoryGridPanel->EquipItem(ActiveGrid, ActiveItem))
		{
			ActiveGrid->SlotMap.Add(ActiveGrid->Slots[ActiveItem->GetInventoryIndex()], false);
			InventoryWidget->InventoryGridPanel->GetInventoryComponent()->ChangeInventoryVolume(0);
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1,2.f, FColor::Yellow, "Not possible!");
		}
	}

	SetVisibility(ESlateVisibility::Hidden);
}

void UItemActionMenu::OnClickDropButton()
{
	InventoryWidget->SetStack(false);
	InventoryWidget->ReadyPickup(false, false, ActiveItem);

	SetVisibility(ESlateVisibility::Hidden);
}

void UItemActionMenu::OnClickDropAllButton()
{
	InventoryWidget->SetStack(true);
	InventoryWidget->ReadyPickup(false, false, ActiveItem);

	SetVisibility(ESlateVisibility::Hidden);
}
