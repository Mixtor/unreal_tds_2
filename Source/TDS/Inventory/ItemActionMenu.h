﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "DynamicInventoryGrid.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"

#include "ItemActionMenu.generated.h"

class UInventoryWidget;
class UInventoryItemDisplay;
/**
 * 
 */
UCLASS()
class TDS_API UItemActionMenu : public UUserWidget
{
	GENERATED_BODY()

public:
	
	UPROPERTY(meta = (BindWidget))
		UButton* UseButton = nullptr;
	UPROPERTY(meta = (BindWidget))
		UButton* EquipButton = nullptr;
	UPROPERTY(meta = (BindWidget))
		UButton* DropButton = nullptr;
    UPROPERTY(meta = (BindWidget))
        UButton* DropAllButton = nullptr;

	UFUNCTION()
		void OnClickUseButton();
	UFUNCTION()
		void OnClickEquipButton();
	UFUNCTION()
		void OnClickDropButton();
	UFUNCTION()
		void OnClickDropAllButton();

	void SetInventoryWidget(UInventoryWidget* Inventory) {InventoryWidget = Inventory;}
	void SetActiveItem(UInventoryItemDisplay* Item) {ActiveItem = Item;}
	void SetActiveGrid(UDynamicInventoryGrid* Grid) {ActiveGrid = Grid;}

protected:
	
	virtual void NativeConstruct() override;

private:

	UPROPERTY()
		UInventoryWidget* InventoryWidget;
	UPROPERTY()
		UInventoryItemDisplay* ActiveItem;
	UPROPERTY()
		UDynamicInventoryGrid* ActiveGrid;
};