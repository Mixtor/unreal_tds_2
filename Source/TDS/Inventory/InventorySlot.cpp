﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "InventorySlot.h"

#include "DragDropWidget.h"
#include "Components/GridPanel.h"
#include "InventoryItemDisplay.h"
// #include "InventoryWidget.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/WidgetTree.h"
#include "Components/Border.h"
// #include "Rendering/DrawElements.h"
#include "Components/CanvasPanelSlot.h"
#include "Components/UniformGridSlot.h"
#include "TDS/Inventory/AdvancedButton.h"

void UInventorySlot::NativeConstruct()
{
	Super::NativeConstruct();

	TArray<UUserWidget*> Inventory;
	UWidgetBlueprintLibrary::GetAllWidgetsOfClass(this, Inventory, UInventoryWidget::StaticClass(), false);
	InventoryWidget = Cast<UInventoryWidget>(Inventory[0]);

	if(SlotBorder)
	{
		WorkingColor = Owner->SelectionColor.BorderInitialColor;
	}

	if(Owner == InventoryWidget->ModifierGridPanel)
	{
		UCanvasPanelSlot* SlotCanvas = Cast<UCanvasPanelSlot>(SlotBorder->Slot);
		const FVector2D ActualSize = SlotCanvas->GetSize();
		SlotCanvas->SetSize({ActualSize.X - 100, ActualSize.Y - 100});
	}
}

void UInventorySlot::SetCoordinate(const int RowNo, const int ColumnNo)
{
	Row = RowNo;
	Column = ColumnNo;
}

bool UInventorySlot::NativeOnDrop(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation)
{
	Super::NativeOnDrop(InGeometry, InDragDropEvent, InOperation);

	UDragDropWidget* DropWidget = Cast<UDragDropWidget>(InOperation);

	if (DropWidget == nullptr) {return false;}

	if(UUniformGridPanel* GridPanel = Cast<UUniformGridPanel>(GetParent()))
	{
		if(UInventoryItemDisplay* InventoryItemDisplay = Cast<UInventoryItemDisplay>(DropWidget->WidgetToDrag))
		{
			InventoryItemDisplay->Owner->GetInventoryWidget()->SwappedItems.Empty();
			
			if(Owner)
			{	
				InventoryWidget->InventoryGrids.Add(Owner, true);
				
				if(InventoryWidget->InventoryGrids.Num() == 2)
				{
					if(SlotType == InventoryItemDisplay->ItemData->GetPickupType() || SlotType == EIT_Item)
					{
						TMap<UPickupDABase*, bool> Item;

						if(Owner->IsModifierGrid)
						{
							if(SlotType == EIT_Enhancer)
							{
								Item.Add(InventoryItemDisplay->ItemData, true);
								InventoryWidget->Enhancement(Item);
							}
							else
							{
								Item.Add(InventoryItemDisplay->ItemData, true);
								InventoryWidget->Modification(Item);
							}
						}
						else
						{
							if(InventoryItemDisplay->ItemData->GetPickupType() == EIT_Enhancer)
							{
								Item.Add(InventoryItemDisplay->ItemData, false);
								InventoryWidget->Enhancement(Item);
								Owner->GetInventoryComponent()->ChangeInventoryVolume(0);
							}
							else
							{
								Item.Add(InventoryItemDisplay->ItemData, false);
								InventoryWidget->Modification(Item);
							}
						}
						
						InventoryWidget->SwapGrids(InventoryItemDisplay, this);
						return true;
					}
					return false;
				}

				if(Owner->IsItemAvailableForSlot(SlotIndex) && !Owner->IsModifierGrid)
				{
					InventoryItemDisplay->SetVisibility(ESlateVisibility::Visible);

					if(UUniformGridSlot* GridSlot = Cast<UUniformGridSlot>(InventoryItemDisplay->Slot))
					{
						GridSlot->SetColumn(Column);
						GridSlot->SetRow(Row);
					}

					Owner->FillSlots(Owner->GetCoordinateByIndex(SlotIndex));
					InventoryItemDisplay->SetInventoryIndex(SlotIndex);
				}
				else
				{
					bCanDraw = false;
					return false;
				}
			}
		}
	}
	bCanDraw = false;
	return true;
}

/*bool UInventorySlot::NativeOnDragOver(const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation)
{
	Super::NativeOnDragOver(InGeometry, InDragDropEvent, InOperation);
	
	UDragDropWidget* DropWidget = Cast<UDragDropWidget>(InOperation);

	if (DropWidget == nullptr)
	{
		return false;
	}
	if(Owner)
	{
		if(UInventoryItemDisplay* InventoryItemDisplay = Cast<UInventoryItemDisplay>(DropWidget->WidgetToDrag))
		{
			const bool IsSlotValid = Owner->IsItemAvailableForSlot(SlotIndex);

			//DrawSize = {50 * InventoryItemDisplay->GetItemSize()};
			
			Color = IsSlotValid ?  ValidPlaceColor : InvalidPlaceColor;
        }
	}
	else /*if(UInventoryItemDisplay* Item = Cast<UInventoryItemDisplay>(UInventoryItemDisplay::StaticClass()))#1#
	{

	}
	if(UGridPanel* GridPanel = Cast<UGridPanel>(GetParent()))
	{

		bCanDraw = true;
	}

	return true;
}*/

/*int32 UInventorySlot::NativePaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect,
								FSlateWindowElementList& OutDrawElements, int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled) const
{
	Super::NativePaint(Args, AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);

	if (bCanDraw)
	{
		GEngine->AddOnScreenDebugMessage(-1,1.f, FColor::Green, "DRAW");
		FPaintContext Context(AllottedGeometry, MyCullingRect, OutDrawElements, LayerId, InWidgetStyle, bParentEnabled);
		OnPaint( Context );

		//todo handle if box is out of Inventory widget
		
		UWidgetBlueprintLibrary::DrawBox(Context, {0,0}, DrawSize, BrushAsset, Color);
	
		return FMath::Max(LayerId, Context.MaxLayer);
	}
	GEngine->AddOnScreenDebugMessage(-1,1.f, FColor::Red, "NO DRAW");
	return LayerId;
}*/

void UInventorySlot::NativeOnDragLeave(const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation)
{
	Super::NativeOnDragLeave(InDragDropEvent, InOperation);

	bCanDraw = false;
}

void UInventorySlot::NativeOnDragCancelled(const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation)
{
	Super::NativeOnDragCancelled(InDragDropEvent, InOperation);

	bCanDraw = false;
}

void UInventorySlot::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseEnter(InGeometry, InMouseEvent);

	if(SlotBorder)
	{
		WorkingColor = SlotBorder->BrushColor;
		SlotBorder->SetBrushColor(Owner->SelectionColor.OverSlotColor);
	}
}

void UInventorySlot::NativeOnMouseLeave(const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseLeave(InMouseEvent);
	
	if(SlotBorder)
	{
		SlotBorder->SetBrushColor(WorkingColor);
	}
}

FReply UInventorySlot::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	FEventReply Reply;
	Reply.NativeReply = Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);

	if(Owner->IsModifierGrid == false)
	{
		AInventoryBot* InventoryBot = Cast<AInventoryBot>(Owner->GetInventoryComponent()->GetOwner());
		InventoryBot->HideActionMenu();
		
		FEventReply Reply_L;
		FEventReply Reply_R;
		Reply_L.NativeReply = Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
		Reply_R.NativeReply = Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
		Reply_L = UWidgetBlueprintLibrary::DetectDragIfPressed(InMouseEvent,this, FKey{"LeftMouseButton"});
		Reply_R = UWidgetBlueprintLibrary::DetectDragIfPressed(InMouseEvent,this, FKey{"RightMouseButton"});
		
		if(Reply_L.NativeReply.IsEventHandled())
		{
			for(UInventorySlot* SlotToClear : Owner->Slots)
			{
				if(SlotToClear->SlotBorder->BrushColor != Owner->SelectionColor.ActivatedSlotColor)
				{
					SlotToClear->SlotBorder->SetBrushColor(Owner->SelectionColor.BorderInitialColor);
				}
			}
			if(SlotBorder)
			{
				for(auto& SlotToCheck : Owner->GetInventoryWidget()->DropSlots)
				{
					SlotToCheck.Key->SlotBorder->SetBrushColor(Owner->SelectionColor.ActivatedSlotColor);
				}
				SlotBorder->SetBrushColor(Owner->SelectionColor.SelectedSlotColor);
				WorkingColor = Owner->SelectionColor.SelectedSlotColor;
			}
			Owner->SelectedSlot = this;
			Owner->GetInventoryWidget()->ForwardButton->GetParent()->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
			
			Reply.NativeReply = Reply_L.NativeReply;
		}
			
		if(Reply_R.NativeReply.IsEventHandled())
		{
			if(WorkingColor == Owner->SelectionColor.ActivatedSlotColor)
			{
				SlotBorder->SetBrushColor(Owner->SelectionColor.BorderInitialColor);
				WorkingColor = Owner->SelectionColor.BorderInitialColor;
			}
		
			for(auto& SingleSlot : Owner->GetInventoryWidget()->DropSlots)
			{
				if(SingleSlot.Key->GetIndex() == this->SlotIndex)
				{
					for(int i = 0; i < 4; i++)
					{
						if(SingleSlot.Value.ActiveButtons[i])
						{
							Owner->GetInventoryWidget()->WidgetButtons[i]->SetIsEnabled(true);
							Owner->GetInventoryWidget()->SlotNumbers[i]->SetVisibility(ESlateVisibility::Hidden);
						}
					}
					Owner->GetInventoryWidget()->DropSlots.Remove(SingleSlot.Key);
				}
			}
			Reply.NativeReply = Reply_R.NativeReply;
		}
		return Reply.NativeReply;
	}
	return Reply.NativeReply;
}

FReply UInventorySlot::NativeOnMouseButtonUp(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseButtonUp(InGeometry, InMouseEvent);
	
	return FReply::Handled();
}
