﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "InventoryComponent.h"
#include "DynamicInventoryGrid.h"
#include "InventoryWidget.h"
#include "Kismet/GameplayStatics.h"
#include "TDS/Pickup/PickupDABase.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
}

/*// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}*/

void UInventoryComponent::InitializeInventory()
{
	if(APlayerController* PC = UGameplayStatics::GetPlayerController(GetWorld(), 0))
	{
		InventoryWidget = CreateWidget<UInventoryWidget>(PC, InventoryWidgetClass);
		InventoryWidget->AddToViewport();
		InventoryWidget->SetVisibility(ESlateVisibility::Hidden);
		InventoryWidget->InventoryGridPanel->SetInventoryComponent(this);
	}

	// InventoryVolume = InventoryMaxVolume;
	InventoryMaxVolume = InventoryInitialVolume;
	InventoryVolume = InventoryInitialVolume;
	InventoryWidget->VolumeBarState();
}

bool UInventoryComponent::PickUpItem(APickupBase* Pickup)
{
	if(InventoryWidget)
	{
		if(Pickup->ItemData->GetItemVolume() <= InventoryVolume)
		{
			if(InventoryWidget->InventoryGridPanel->AddItem(Pickup->ItemData, Pickup->PickupData, -1))
			{
				ChangeInventoryVolume(Pickup->ItemData->GetItemVolume());
				InventoryWidget->VolumeBarState();

				return true;
			}
		}
	}
	return false;
}

void UInventoryComponent::RemoveItem(UPickupDABase* ItemData)
{
	ItemContainer.Remove(ItemData);

	if(InventoryWidget)
	{
		InventoryWidget->InventoryGridPanel->RemoveItem(ItemData);
	}
}

void UInventoryComponent::ChangeInventoryVolume(const int Volume)
{
	InventoryVolume -= Volume;
	if(InventoryVolume < 0)
	{
		UInventoryItemDisplay* ItemToDrop = nullptr;
		
		int CheckVolume = 0;
		for(auto& ItemToCheck : InventoryWidget->InventoryGridPanel->ItemDisplayContainer)
		{
			const int ItemVolume = ItemToCheck->ItemData->GetItemVolume();
			
			if(ItemVolume > CheckVolume)
			{
				CheckVolume = ItemVolume;
				ItemToDrop = ItemToCheck;
			}
		}
		
		InventoryWidget->ReadyPickup(false, false, ItemToDrop);
		ChangeInventoryVolume(0);
	}
}
