﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"

#include "ItemInfo.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UItemInfo : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(meta = (BindWidget))
		UTextBlock* Name = nullptr;
	UPROPERTY(meta = (BindWidget))
		UTextBlock* Description = nullptr;
	UPROPERTY(meta = (BindWidget))
		UTextBlock* Volume = nullptr;
};
