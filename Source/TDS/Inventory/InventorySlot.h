﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InventoryEnums.h"
#include "InventoryWidget.h"
#include "Blueprint/UserWidget.h"

#include "InventorySlot.generated.h"

class USlateBrushAsset;
class UDynamicInventoryGrid;
class UBorder;
/**
 * 
 */
UCLASS()
class TDS_API UInventorySlot : public UUserWidget
{
	GENERATED_BODY()
	
public:
	
	UPROPERTY(meta = (BindWidget))
		UBorder* SlotBorder = nullptr;
	UPROPERTY(meta = (BindWidget))
		UImage* SlotImage = nullptr;
	UPROPERTY(EditDefaultsOnly, Category = "ModifierGridSlots")
		FSlateBrush EnhancerSlot;
	UPROPERTY(EditDefaultsOnly, Category = "ModifierGridSlots")
		FSlateBrush ModifierSlot;
	
	UPROPERTY(EditDefaultsOnly)
		USlateBrushAsset* BrushAsset = nullptr;
	UPROPERTY(EditDefaultsOnly)
		FLinearColor ValidPlaceColor;
	UPROPERTY(EditDefaultsOnly)
		FLinearColor InvalidPlaceColor;

	UPROPERTY()
		UDynamicInventoryGrid* Owner = nullptr;

	EItemType SlotType;

	void SetCoordinate(const int RowNo, const int ColumnNo);
	void SetIndex(const int Index) {SlotIndex = Index;}
	int GetIndex() const {return SlotIndex;}

	FIntPoint GetSlotCoordinates() const {return FIntPoint {Column, Row};} 
	
private:
	
	bool bCanDraw = false;
	int SlotIndex = -1;
	int Row = -1;
	int Column = -1;

	FVector2D DrawSize = FVector2D::ZeroVector;

	FLinearColor Color;

	UPROPERTY()
		FLinearColor WorkingColor;

	UPROPERTY()
		UInventoryWidget* InventoryWidget;
	
	virtual void NativeConstruct() override;

	virtual bool NativeOnDrop( const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation ) override;
	// virtual bool NativeOnDragOver( const FGeometry& InGeometry, const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation ) override;

	/*virtual int32 NativePaint(const FPaintArgs& Args, const FGeometry& AllottedGeometry, const FSlateRect& MyCullingRect,FSlateWindowElementList& OutDrawElements,
							int32 LayerId, const FWidgetStyle& InWidgetStyle, bool bParentEnabled ) const override;*/
	
	virtual void NativeOnDragLeave( const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation ) override;
	virtual void NativeOnDragCancelled( const FDragDropEvent& InDragDropEvent, UDragDropOperation* InOperation ) override;
	
	virtual void NativeOnMouseEnter( const FGeometry& InGeometry, const FPointerEvent& InMouseEvent ) override;
	virtual void NativeOnMouseLeave( const FPointerEvent& InMouseEvent ) override;

	virtual FReply NativeOnMouseButtonDown( const FGeometry& InGeometry, const FPointerEvent& InMouseEvent ) override;
	virtual FReply NativeOnMouseButtonUp( const FGeometry& InGeometry, const FPointerEvent& InMouseEvent ) override;
};
