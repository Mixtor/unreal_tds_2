// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"

#include "Item_Energy.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UItem_Energy : public UItem
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item", meta = (ClampMin = 1.0))
	float HealthToHeal;
	
protected:

	virtual void Use(class ATDSCharacter* Character) override;
	
};
