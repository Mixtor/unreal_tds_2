// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "GameplayTagContainer.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "WeaponDefaults.h"
#include "TDSInventoryComponent.h"
#include "Inventory/InventoryBot.h"
#include "CharacterHealthComponent.h"
#include "Interface/TDS_IGameActor.h"
// #include "StateEffect.h"
//#include "Components/WidgetComponent.h"

#include "TDSCharacter.generated.h"

class SGridPanel;

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter, public ITDS_IGameActor
{
	GENERATED_BODY()

protected:
	
	virtual void BeginPlay() override;

public:
	
	ATDSCharacter();

	FTimerHandle TimerHandle_RagDollTimer;
	
	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent SubObject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const {return TopDownCameraComponent;}
	/** Returns CameraBoom SubObject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const {return CamBoom;}
	// Add Inventory
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory, meta = (AllowPrivateAccess = "true"))
		class UTDSInventoryComponent* InventoryComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Inventory)
		USphereComponent* PerceptionSphere = nullptr;

	UPROPERTY(BlueprintReadWrite, Category = Inventory)
		AInventoryBot* InventoryBot = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
		class UCharacterHealthComponent* CharacterHealthComponent;

	
private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CamBoom;
	
	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;

public:
	//-------------------------------------------------------------------------------------- Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	//-------------------------------------------------------------------------------------- Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementSpeedInfo;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool Stand = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool Aim = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool Walk = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool Sprint = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool Fatigue = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float Stamina = 3.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float Restitution = 3.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bIsAlive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		TArray<UAnimMontage*> DeathAnim;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float MaxHealth = 100.f;
	UPROPERTY(BlueprintReadWrite, Category = "Health")
		float Health;
	
	UFUNCTION(BlueprintCallable, Category = "Items")
		void UseItem(class UItem* Item);
	
	//-------------------------------------------------------------------------------------- Input
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();

	float AxisX = 0.0f;
	float AxisY = 0.0f;
	
	//-------------------------------------------------------------------------------------- Weapon
	UPROPERTY()
		AWeaponDefaults* CurrentWeapon = nullptr;
	UPROPERTY()
		AWeaponDefaults* LeftWeapon = nullptr;
	UPROPERTY()
		AWeaponDefaults* RightWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		bool Armed = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		bool HeavyArmed = false;

	//-------------------------------------------------------------------------------------- Effect
	UPROPERTY()
		TArray<UStateEffect*> Effects;

	//-------------------------------------------------------------------------------------- For Demo
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;

	UPROPERTY(BlueprintReadOnly)
		UDecalComponent* CurrentCursor = nullptr;

	//-------------------------------------------------------------------------------------- Tick Function
	UFUNCTION(BlueprintCallable)
		void MovementTick(float DeltaTime);

	//-------------------------------------------------------------------------------------- Func
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();

	UFUNCTION(Blueprintcallable)
		AWeaponDefaults* GetCurrentWeapon();
	UFUNCTION(Blueprintcallable)
		AWeaponDefaults* SetCurrentWeapon(bool Gun);
//..............................................
	UFUNCTION(BlueprintCallable)
		UPARAM (DisplayName="Armed") bool InitWeapon(FName WeaponID, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);

	//UFUNCTION(BlueprintCallable)
	//	void InitWeapon(FName WeaponID, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);

	UFUNCTION(BlueprintCallable)//VisualOnly
		void RemoveCurrentWeapon();
//..............................................
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim, USoundBase* Sound);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccessful, int32 AmmoTake);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim, USoundBase* Sound);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccessful);
	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	//-------------------------------------------------------------------------------------- Inventory
	bool IsInventoryActive = false;

	UPROPERTY(BlueprintReadOnly)
		FTimerHandle BotSearchHandle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Inventory)
		float BotSearchPeriod = 2.f;
	
	void CallInventory();

	void TrySwitchNextWeapon();
	void TrySwitchPreviousWeapon();
	void ResetBotSearch();
	
	UFUNCTION()
		void AcquireBot(AInventoryBot* Bot);
	
	UFUNCTION(BlueprintCallable)
		void EmergencyCheck();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentWeaponIndex = 0;

	//-------------------------------------------------------------------------------------- Fatigue & Rest
	FTimerHandle FatigueHandle;
	FTimerHandle RestHandle;
	UFUNCTION()
		void OnFatigue();
	UFUNCTION()
		void OnRest();

	//-------------------------------------------------------------------------------------- Interface
	EPhysicalSurface GetSurfaceType() override;
	TArray<UStateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UStateEffect* RemoveEffect)override;
	void AddEffect(UStateEffect* NewEffect)override;
	
	//-------------------------------------------------------------------------------------- Character take Damage & Death of the Character
	UFUNCTION()
		void CharDeath();
	
	void EnableRagDoll();
	// virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
};