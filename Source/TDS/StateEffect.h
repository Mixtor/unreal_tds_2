﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"

#include "StateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class TDS_API UStateEffect : public UObject
{
	GENERATED_BODY()

public:
	
	virtual bool InitObject(FHitResult Hit);
	virtual void DestroyObject();

	UFUNCTION(BlueprintNativeEvent)
		void ApplyEffect(FHitResult Target);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		bool bIsStakable = false;
	
	UPROPERTY()
		AActor* MyActor = nullptr;

	FHitResult HitResult;
};

UCLASS()
class TDS_API UStateEffect_ExecuteOnce : public UStateEffect
{
	GENERATED_BODY()

public:
	
	bool InitObject(FHitResult Hit) override;
	void DestroyObject() override;	

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float Power = 20.0f;
};

UCLASS()
class TDS_API UStateEffect_ExecuteTimer : public UStateEffect
{
	GENERATED_BODY()

public:
	
	bool InitObject(FHitResult Hit) override;
	void DestroyObject() override;
	
	virtual void Execute();

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float TimeRate = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		UParticleSystem* ParticleEffect = nullptr;

	UPROPERTY()
		UParticleSystemComponent* ParticleEmitter = nullptr;
};
