﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "EnvironmentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
AEnvironmentStructure::AEnvironmentStructure()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AEnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AEnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

EPhysicalSurface AEnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = SurfaceType_Default;
	UStaticMeshComponent* MyMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));

	if(MyMesh)
	{
		UMaterialInterface* MyMaterial = MyMesh->GetMaterial(0);
		
		if(MyMaterial)
		{
			Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
			GEngine->AddOnScreenDebugMessage(-1,1.f, FColor::Red, "MATERIAL");
		}
	}
	return Result;
}
TArray<UStateEffect*> AEnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void AEnvironmentStructure::RemoveEffect(UStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void AEnvironmentStructure::AddEffect(UStateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}


