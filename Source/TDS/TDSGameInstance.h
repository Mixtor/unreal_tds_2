
#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Types.h"
#include "Engine/DataTable.h"
#include "Inventory/InventoryBot.h"
//#include "WeaponDefaults.h"

#include "TDSGameInstance.generated.h"

USTRUCT(BlueprintType)
struct FInventoryBotStruct
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
		TArray<UInventoryItemDisplay*> ModifierItems;
	UPROPERTY(BlueprintReadWrite)
		TArray<UInventoryItemDisplay*> InventoryItems;
	UPROPERTY(BlueprintReadWrite)
		TArray<int> ItemsNumber;
};
/**
 * 
 */
UCLASS()
class TDS_API UTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	// Table
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
		UDataTable* WeaponInfoTable = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
		UDataTable* DropItemInfoTable = nullptr;

	UPROPERTY(BlueprintReadWrite)
		FInventoryBotStruct InventoryBotInfo;

	UPROPERTY(BlueprintReadWrite)
		TMap<int, FInventoryBotStruct> InventoryBotCollection;
	
	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName WeaponName, FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool GetDropItemInfoByName(FName ItemName, FDropItem& OutInfo);

	void SetBotCatalog();
	int GetBotCatalog() const {return BotCatalog;}
	
private:

	int BotCatalog = 0;
};
