﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "CharacterHealthComponent.h"
#include "TDSCharacter.h"

float UCharacterHealthComponent::GetCurrentShield()
{
	return Shield; 
}

void UCharacterHealthComponent::ChangeShieldValue(const float ChangeValue)
{
	Shield += ChangeValue;

	if(Shield > 100.0f) {Shield = 100.0f;}
	else if(Shield < 0.0f) {Shield = 0.0f;}

	if(GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShieldTimer, this, &UCharacterHealthComponent::CoolDownShieldEnd,
											CoolDownShieldRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}	

	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UCharacterHealthComponent::CoolDownShieldEnd()
{
	if(GetWorld())
	{		
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}	
}

void UCharacterHealthComponent::RecoveryShield()
{
	float TempValue = Shield;
	TempValue = TempValue + ShieldRecoverValue;

	if(TempValue > 100.0f)
	{
		Shield = 100.0f;
		if(GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else Shield = TempValue;

	OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}

void UCharacterHealthComponent::ChangeHealthValue(const float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	Super::ChangeHealthValue(ChangeValue);

	ATDSCharacter* MyCharacter = Cast<ATDSCharacter>(GetOwner());
	
	if(MyCharacter->InventoryBot && ChangeValue < 0)
	{
		MyCharacter->EmergencyCheck();
	}
	
	// if(Shield > 0.0f && ChangeValue < 0.0f)
	// {
	// 	ChangeShieldValue(ChangeValue);
	// 	
	// 	if(Shield < 0.0f)
	// 	{
	// 		//FX
	// 		//UE_LOG(LogTemp, Warning, TEXT("UTPSCharacterHealthComponent::ChangeHealthValue - Sheild < 0"));
	// 	}
	// }
	// else
	// {
	// 	Super::ChangeHealthValue(ChangeValue);
	// }
}
