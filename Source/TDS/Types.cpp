// Fill out your copyright notice in the Description page of Project Settings.

#include "Types.h"
// #include "TDS.h"
#include "Interface/TDS_IGameActor.h"


void UTypes::AddEffectBySurfaceType(FHitResult Hit, TSubclassOf<UStateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	AActor* TakeEffectActor = Cast<AActor>(Hit.Actor);
	
	if (SurfaceType != SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UStateEffect* StateEffect = Cast<UStateEffect>(AddEffectClass->GetDefaultObject());
		
		if (StateEffect)
		{
			bool bIsPossibleSurface = false;
			int8 i = 0;

			while(i < StateEffect->PossibleInteractSurface.Num() && !bIsPossibleSurface)
			{
				if(StateEffect->PossibleInteractSurface[i] == SurfaceType)
				{			
					bIsPossibleSurface = true;
					bool bIsCanAddEffect = false;
					
					if(!StateEffect->bIsStakable)
					{
						int8 j = 0;
						TArray<UStateEffect*> CurrentEffects;
						ITDS_IGameActor* Interface = Cast<ITDS_IGameActor>(TakeEffectActor);
						
						if(Interface)
						{
							CurrentEffects = Interface->GetAllCurrentEffects();
						}

						if(CurrentEffects.Num() > 0)
						{
							while(j < CurrentEffects.Num() && !bIsCanAddEffect)
							{
								if(CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}
					}
					else
					{
						bIsCanAddEffect = true;
					}

					if(bIsCanAddEffect)
					{
						UStateEffect* NewEffect = NewObject<UStateEffect>(TakeEffectActor, AddEffectClass);
						
						if(NewEffect)
						{
							NewEffect->InitObject(Hit);
						}
					}
				}
				i++;
			}
		}
	}
}

