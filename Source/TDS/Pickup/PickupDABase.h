﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TDS/Inventory/InventoryEnums.h"

#include "PickupDABase.generated.h"

/**
* 
*/

UCLASS()
class TDS_API UPickupDABase : public UDataAsset
{
	GENERATED_BODY()

public:
	
	UFUNCTION(BlueprintCallable)
	    int GetId() const {return Id;}

	UFUNCTION(BlueprintCallable)
		EItemType GetPickupType() const {return PickupType;}
	
	UFUNCTION(BlueprintCallable)
	    const FText& GetItemName() const {return ItemName;}
	
	UFUNCTION(BlueprintCallable)
	    const FString& GetDescription() const {return Description;}
	
	UFUNCTION(BlueprintCallable)
		float GetItemValue() const {return ItemValue;}
	
	UFUNCTION(BlueprintCallable)
		int GetItemVolume() const {return ItemVolume;}
	
	UFUNCTION(BlueprintCallable)
	    UTexture2D* GetThumbnail() const {return ItemIcon;}
	
	UFUNCTION(BlueprintCallable)
	    UStaticMesh* GetMeshComponent() const {return ItemMesh;}

	UFUNCTION(BlueprintCallable)
	   TSubclassOf<AActor> GetComponent() const {return Component;}

	UFUNCTION(BlueprintCallable)
		bool CheckStackable() const {return Stackable;}

private:

	UPROPERTY()
		int Id = GetUniqueID();

	UPROPERTY(EditAnywhere)
		TEnumAsByte<EItemType> PickupType;
	
	UPROPERTY(EditAnywhere)
		FText ItemName;

	UPROPERTY(EditAnywhere)
		FString Description;

	UPROPERTY(EditAnywhere)
		float ItemValue = 0;

	UPROPERTY(EditAnywhere)
		int ItemVolume = 0;
	
	UPROPERTY(EditAnywhere)
		UTexture2D* ItemIcon;

	UPROPERTY(EditAnywhere)
		UStaticMesh* ItemMesh;

	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "PickupType == EItemType::EIT_Item", EditConditionHides))
		bool Stackable = false;
	
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "PickupType == EItemType::EIT_Modifier", EditConditionHides))
		TSubclassOf<AActor>  Component;
};

