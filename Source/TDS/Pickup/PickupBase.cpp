﻿
#include "PickupBase.h"

#include "PickupDABase.h"
#include "Components/DecalComponent.h"
#include "Components/SphereComponent.h"
#include "TDS/TDSCharacter.h"
// #include "TDS/Inventory/InventorySphere.h"

// Sets default values
APickupBase::APickupBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SC"));
	RootComponent = SceneComponent;
	
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MC"));
	MeshComponent->SetupAttachment(SceneComponent);
	MeshComponent->SetRelativeLocation({0,0,32});
	
	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("C"));
	Collision->SetupAttachment(MeshComponent);
	Collision->SetSphereRadius(32);

	Decal = CreateDefaultSubobject<UDecalComponent>(TEXT("D"));
	Decal->SetupAttachment(SceneComponent);
	Decal->SetRelativeRotation({-90,0,0});
	Decal->SetRelativeScale3D({0.25,0.25,0.25});

	FX = CreateDefaultSubobject<UNiagaraComponent>(TEXT("NS"));
	FX->SetupAttachment(SceneComponent);

	ItemCounter = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TRC"));
	ItemCounter->SetupAttachment(SceneComponent);
	ItemCounter->SetRelativeLocation({0,0,10});
	ItemCounter->SetRelativeRotation({90,180,0});
	ItemCounter->SetHorizontalAlignment(EHTA_Center);
	ItemCounter->SetVerticalAlignment(EVRTA_TextCenter);
	ItemCounter->SetTextRenderColor(FColor::Red);
	ItemCounter->SetWorldSize(60);

	RotatingMovement = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("RMC"));
}

// Called when the game starts or when spawned
void APickupBase::BeginPlay()
{
	Super::BeginPlay();

	AssignCollisionChannel();
	UpdateComponent();
	ItemCounter->SetText(FText::AsNumber(PickupStack));
	
	if(PickupStack == 1) {ItemCounter->SetVisibility(false);}
}

int APickupBase::SetPickupStack(const int Item)
{
	PickupStack += Item;

	return PickupStack;
}

void APickupBase::AssignCollisionChannel() const
{
	Collision->SetCollisionObjectType(ECC_GameTraceChannel3); // ECC_GameTraceChannel3 = Pickup
}

void APickupBase::UpdateComponent() const
{
	RotatingMovement->SetUpdatedComponent(MeshComponent);
}

void APickupBase::OnConstruction(const FTransform& Transform)
{
	InitItemActor();
}

void APickupBase::InitItemActor()
{
	if(ItemData)
	{
		if(ItemData->GetMeshComponent())
		{
			UStaticMesh& SM = *ItemData->GetMeshComponent();
		
			MeshComponent->SetStaticMesh(&SM);
			MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

			FX->SetAsset(PickupData.Niagara);
			Decal->SetDecalMaterial(PickupData.Decal);
			Collision->SetSphereRadius(PickupData.CollisionRadius);

			Collision->OnComponentBeginOverlap.AddDynamic(this, &APickupBase::OnOverlapSomething);
			
			RotatingMovement->RotationRate = PickupData.ItemRotation;
			PickupValue = ItemData->GetItemValue();
			PickupVolume = ItemData->GetItemVolume();
			if(PickupData.Decal == nullptr)
			{
				Decal->SetVisibility(false);
			}
			else Decal->SetVisibility(true);
		}
	}
}

void APickupBase::OnOverlapSomething(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
									const FHitResult& SweepResult)
{
	if(OtherComp->ComponentHasTag("Collector") && Collectable)
	{
		AInventoryBot* InventoryBot = Cast<AInventoryBot>(OtherActor);
		const int Stack = GetPickupStack();
		
		for(int i = Stack; i > 0; i--)
		{
			if(InventoryBot->BotInventoryComponent->PickUpItem(this))
			{
				if(SetPickupStack(-1) == 0) {Destroy();}
				else if (GetPickupStack() == 1) {ItemCounter->SetVisibility(false);}

				ItemCounter->SetText(FText::AsNumber(PickupStack));
			}
			else i = 0;
		}
	}
	
	if(OtherActor->ActorHasTag("Player"))
	{
		if(this->ItemData->GetPickupType() == EIT_Item)
		{
			ATDSCharacter* Character = Cast<ATDSCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());

			if(Character->CharacterHealthComponent->GetCurrentHealth() < Character->CharacterHealthComponent->GetMaxHealth())
			{
				Character->CharacterHealthComponent->ChangeHealthValue(this->PickupValue);

				if(SetPickupStack(-1) == 0) {Destroy();}
				else if (GetPickupStack() == 1) {ItemCounter->SetVisibility(false);}

				ItemCounter->SetText(FText::AsNumber(PickupStack));
			}
		}
	}
}


