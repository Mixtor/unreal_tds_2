﻿#pragma once

#include "CoreMinimal.h"
#include "NiagaraComponent.h"
#include "Components/SphereComponent.h"
#include "Components/TextRenderComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/RotatingMovementComponent.h"

#include "PickupBase.generated.h"

class UPickupDABase;
class UStaticMesh;
class USphereComponent;

USTRUCT(BlueprintType)
struct FPickupDataStruct
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = "Pickup Data")
		float CollisionRadius = 32;
	UPROPERTY(EditDefaultsOnly, Category = "Pickup Data")
		FRotator ItemRotation = {0, 0, 0};
	UPROPERTY(EditDefaultsOnly, Category = "Pickup Data")
		UMaterial* Decal = nullptr;
	UPROPERTY(EditDefaultsOnly, Category = "Pickup Data")
		UNiagaraSystem* Niagara = nullptr;
};

UCLASS()
class TDS_API APickupBase : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default Values for this Actor's Properties
	APickupBase();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item Properties")
		UPickupDABase* ItemData = nullptr;
	UPROPERTY(EditDefaultsOnly, Category = "Pickup Properties")
		FPickupDataStruct PickupData;

	UPROPERTY()
		UTextRenderComponent* ItemCounter = nullptr;
	
	UPROPERTY(BlueprintReadOnly)
		float PickupValue = 0;
	UPROPERTY(BlueprintReadOnly)
		int PickupVolume = 0;

	UFUNCTION(BlueprintCallable)
		void InitItemActor();
	UFUNCTION(BlueprintCallable)
		void SetCollectable(bool Collect) {Collectable = Collect;}
	UFUNCTION(BlueprintCallable)
		bool IsCollectable() {return Collectable;}

	UFUNCTION(BlueprintCallable)
		int SetPickupStack (const int Item);
	UFUNCTION(BlueprintCallable)
		int GetPickupStack () const {return PickupStack;}
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
private:
	// EditInstanceOnly
	bool Collectable = true;

	UPROPERTY(EditAnywhere, Category = "Pickup Properties", meta = (ClampMin = "1"))
		int PickupStack = 1;
	
	UPROPERTY()
		USceneComponent* SceneComponent = nullptr;
	UPROPERTY(BlueprintGetter = GetItemMesh)
		UStaticMeshComponent* MeshComponent = nullptr;
	UPROPERTY(BlueprintGetter = GetItemCollision)
		USphereComponent* Collision = nullptr;
	UPROPERTY()
		UDecalComponent* Decal = nullptr;
	UPROPERTY()
		UNiagaraComponent* FX = nullptr;
	UPROPERTY(BlueprintGetter = GetItemRotatingMovement)
		URotatingMovementComponent* RotatingMovement = nullptr;

	UFUNCTION()
		void AssignCollisionChannel() const;
	UFUNCTION()
		void UpdateComponent() const;

	// Accessing the Actor Components in BP
	UFUNCTION(BlueprintGetter)
		USphereComponent* GetItemCollision() const {return Collision;}
	UFUNCTION(BlueprintGetter)
		UStaticMeshComponent* GetItemMesh() const {return MeshComponent;}
	UFUNCTION(BlueprintGetter)
		URotatingMovementComponent* GetItemRotatingMovement() const {return RotatingMovement;}
	
	virtual void OnConstruction(const FTransform& Transform) override;

	UFUNCTION()
		void OnOverlapSomething(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
								const FHitResult& SweepResult);
};