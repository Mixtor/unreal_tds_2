// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Item.generated.h"

/**
 * 
 */
UCLASS(Abstract, BlueprintType, Blueprintable, EditInlineNew, DefaultToInstanced)
class TDS_API UItem : public UObject
{
	GENERATED_BODY()

	public:

		UItem();

		virtual class UWorld* GetWorld() const {return World;};

		UPROPERTY(Transient)
			class UWorld* World;
	
		// Text for using the Item (Equip, Eat etc)
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
			FText UseActionText;
		// Mesh to display for this Item Pickup
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
			class UStaticMesh* PickupMesh;
		// Thumbnail for this Item
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
			class UTexture2D* Thumbnail;
		// Display Name for this Item in the Inventory
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
			FText ItemDisplayName;
		// Optional Description for the Item
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item", meta = (Multiline = true))
			FText ItemDescription;
		// 'Weight' of the Item
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item", meta = (ClampMin = 1.0))
			float Weight;
		// Inventory that owns this Item
		UPROPERTY()
			class UTDSInventoryComponent* OwningInventory;

		virtual void Use(class ATDSCharacter* Character);

		UFUNCTION(BlueprintImplementableEvent)
			void OnUse(class ATDSCharacter* Character);
};
