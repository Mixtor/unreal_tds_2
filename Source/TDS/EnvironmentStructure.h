﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/TDS_IGameActor.h"
#include "StateEffect.h"

#include "EnvironmentStructure.generated.h"

UCLASS()
class TDS_API AEnvironmentStructure : public AActor, public ITDS_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;	
	
	TArray<UStateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UStateEffect* RemoveEffect)override;
	void AddEffect(UStateEffect* NewEffect)override;

	//Effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		TArray<UStateEffect*> Effects;
};